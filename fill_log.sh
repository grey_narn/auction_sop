#!/bin/bash

function write_header {
    echo -e "Fname;Npts;TotalMassA;TotalMassB;Etime;MemM" >> $1
}

# $1 = exec name
# $2 = test_dir
# $3 = log file
# $4 = start_idx
# $5 = end_idx
# $6 = answer file postfix


exec_name=$1
test_dir=$2
log_file=$3
start_idx=$4
end_idx=$5
ans_postfix=$6
#either sop or check_sop 
test_file_prefix=$7
wasser_power=$8
if [ -n "$log_file" ];
then
    write_header $log_file
    for i in `seq $start_idx $end_idx`;
    do
        if [ $i -lt 10 ]; then
            i="0$i"
        fi
        test_file="${test_dir}/${test_file_prefix}_test_$i"
        ./exec_auction.sh "$exec_name" "$test_file" "$log_file" "$ans_postfix" "$wasser_power"
    done
fi
