#!/usr/bin/python
import random
import glob

def make_sop_test_file(fname_in, fname_out, fname_out_sop):
    f_in = open(fname_in, 'r')
    f_out = open(fname_out, 'w')
    f_out_sop = open(fname_out_sop, 'w')
    for line in f_in:
        m = random.randint(10, 1000)
        for k in xrange(0, m):
            f_out.write(line)
        line_sop = line.rstrip() + " " + str(m) + "\n"
        f_out_sop.write(line_sop)
    f_in.close()
    f_out.close()


def make_normal_test_file(fname_in, fname_out):
    f_in = open(fname_in, 'r')
    f_out = open(fname_out, 'w')
    for line in f_in:
        [a, b, c] = line.split(' ')
        m = int(c)
        print a + ' ' + b
        lines = [ a + ' ' + b + "\n" for r in xrange(0,m) ]
        f_out.writelines(lines)
    f_in.close()
    f_out.close()

sop_dir = 'sop_tests/gauss_distr_2/'
file_list = glob.glob("/home/narn/projects/bottleneck/tests/gauss_distr_2/test_??_A")
for fname_in in file_list:
    fname_A = fname_in.split('/')[-1]
    fname_sop_A = sop_dir + 'sop_' + fname_A
    fname_usual_A = sop_dir + 'check_sop_' + fname_A
    make_sop_test_file(fname_in, fname_usual_A, fname_sop_A)
    fname_B = fname_A[:-1] + "B"
    fname_sop_B = sop_dir + 'sop_' + fname_B
    fname_usual_B = sop_dir + 'check_sop_' + fname_B
    make_sop_test_file(fname_in[:-1]+"B", fname_usual_B, fname_sop_B)

