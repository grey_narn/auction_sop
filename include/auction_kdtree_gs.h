#ifndef AUCTION_KDTREE_GS_H
#define AUCTION_KDTREE_GS_H

//#define GATHER_STAT_KDTREE
//#define KEEP_UNASSIGNED_ORDERED 

#include <list>
#include <vector>
#include <set>
#include <memory>
#include <fstream>
#include <unordered_set>
#include <unordered_map>
#include <functional>

#include "basic_defs.h"
#include "my_multiset.h"
#include "bid_table.h"
#include "stat_keeper.h"

#include <dnn/geometry/euclidean-fixed.h>
#include <dnn/local/kd-tree.h>




namespace auction_sop
{

#ifdef KEEP_UNASSIGNED_ORDERED
using IdxPointPair = std::pair<size_t, DiagramPoint>;

struct LexicogrCompDiagramPoint {
    // mass is not taken into account!
    bool operator ()(const IdxPointPair& a, const IdxPointPair& b) {
        const auto& p1 = a.second;
        const auto& p2 = b.second;

        return ( (not p1.isDiagonal() and p2.isDiagonal()) or
                ( p1.isDiagonal() == p2.isDiagonal() and p1.getRealX() < p2.getRealX() ) or
                ( p1.isDiagonal() == p2.isDiagonal() and p1.getRealX() == p2.getRealX() and p1.getRealY() < p2.getRealY() ) or
                ( p1.isDiagonal() == p2.isDiagonal() and p1.getRealX() == p2.getRealX() and p1.getRealY() == p2.getRealY() and a.first < b.first ) );
    }
};

using OrderedUnassignedKeeper = std::set<IdxPointPair, LexicogrCompDiagramPoint>;
#endif


using PointSliceIter = PointSliceHeapAsc::iterator;

using SliceIdxToIterMap = std::unordered_map<SliceIdx, PointSliceIter, SliceIdxHash>;

class AuctionKdtreeRunnerGS{
public:

    // types
    typedef dnn::Point<2, double> DnnPoint;
    typedef dnn::PointTraits<DnnPoint> DnnTraits;
    typedef DnnTraits::PointHandle DnnPointHandle;

    static constexpr size_t NO_OWNER_IDX = std::numeric_limits<size_t>::max(); 

    AuctionKdtreeRunnerGS(std::vector<DiagramPoint>& A,
            std::vector<DiagramPoint>& B,
            double _wassersteinPower,
            double _delta,                                 // relative error
            const std::string fnameStat = std::string(""), // file to write statistics to, "" --- no stats
            const double _initialEpsilon = 0.0,           // initial value for epsilon; 0.0 -> proportional to maxVal
            const double _epsFactor = 0.0);               // factor for epsilon-scaling; 0.0 -> initialEpsToMaxValFactor
    ~AuctionKdtreeRunnerGS() {  }
    // data members
    // for statistics, should be initialized first!
    StatKeeper statKeeper;
    
    dnn::KDTree<DnnTraits>* kdtree;
    size_t numNormalBidders;
    size_t numBidders;
    size_t numNormalObjects;
    size_t numObjects;
    MassType totalBidderMass { 0 };
    MassType totalObjectMass { 0 };
    MassType diagBidderMass { 0 };
    MassType diagObjectMass { 0 };
    MassType totalMass { 0 };

    double epsilon;
    double wassersteinPower;
    double maxRelError;
    double maxVal;
    double minPersistence;
    double minPossibleEpsilon { 0.0 };
    const int maxIterNum { 20 };
    double initialEpsToMaxValFactor { 4.0 };

    double wassersteinDistance;
    double initialEpsilon;
    double epsilonCommonRatio;
    
    std::vector<DiagramPoint> allBidders, allObjects;
    std::vector<DnnPoint> dnnPoints; // for all objects TODO think about that
    std::vector<DnnPointHandle> dnnPointHandles;
    std::vector<DnnPoint> dnnBidders; 

    // additional data structures 
    // they are changed at every iteration
#ifdef KEEP_UNASSIGNED_ORDERED
    MyOrderedMultiset<IdxPointPair, LexicogrCompDiagramPoint> unassignedBidders;
#else
    MyMultiset<MassType> unassignedBidders;
#endif
    std::vector<SliceIdxSet> slicesAssignedToBidder;
    std::vector<PointSliceHeapAsc> objectSlices;
    SliceIdxToIterMap localSliceIdxToIterMap;
    PointSliceHeapDiag globalSlices;
    SliceIdxToIterMap globalSliceIdxToIterMap;
    int numRounds { 0 };
    std::vector<double> persistences;
    std::unordered_set<size_t> normalObjectsWithChangedPrices;

    // methods
    // initialization routines
    void initKDTree();
    void initGlobalHeap();
    // basic methods
    double getWassersteinDistance();
    void runAuction();
    void runAuctionPhase();
    void submitBids(MyMultiset<MassType>::PairType unassignedPair);
    void prepareBidTableDiagonal(size_t bidderIdx, MassType unassignedMass, std::vector<Bid>& bidTable, double& w, MyMultiset<size_t> objSet);
    void prepareBidTableDiagonal(size_t bidderIdx, MassType unassignedMass, BidTable& bidTable);
    void reassignSlices(const size_t bidderIdx, const MassType unassignedMass, std::vector<Bid>& bidTable, double w);
    void reassignSlices(const size_t bidderIdx, const MassType unassignedMass, BidTable& bidTable);
    void flushAssignment();
    // helper routines
    double getDistanceToQthPowerInternal();
    void setSlicePrice(const PointSlice& newPointSlice, const double newPrice);
    void createAssignedSlice(const size_t bidderIdx, const size_t objectIdx, const MassType mass, const double price);
    void eraseSlice(const PointSlice sliceIdx, bool changeUnassigned);
    void addBidToProjectionPreimage(size_t bidderIdx, MassType unassignedMass, BidTable& bidTable);
    void changeSliceMass(const SliceIdx& sliceIdx, MassType delta);
    bool sliceExists(const SliceIdx& sliceIdx) const;
    double getDistToBidder(const size_t bidderIdx, size_t objectIdx) const;
    double getLossForBiddder(const size_t bidderIdx, const PointSlice& slice) const;
    PointSliceIter getLocalSliceIter(const PointSlice& slice);
    PointSliceIter getLocalSliceIter(const SliceIdx& slice);
    PointSliceIter getGlobalSliceIter(const PointSlice& slice);
    PointSliceIter getGlobalSliceIter(const SliceIdx& slice);
    double getSlicePrice(const SliceIdx& slice);
    MassType getSliceMass(const SliceIdx& slice) { return getLocalSliceIter(slice)->getMass(); }
    void stealMassFromSlice(const PointSlice& sliceIdx, MassType delta);
    bool isDiagonal(const PointSlice& slice) const;
    bool isDiagonal(const SliceIdx& slice) const;
    // for debug
    void sanityCheck();
    void checkPerfectMatching();
    friend std::ostream& operator<<(std::ostream& output, AuctionKdtreeRunnerGS&);
    //void printBidHeap(std::ostream& output, size_t bidderIdx);
    double getErrorBound();
    double getLowerBound();
    double getMinEpsilon() const;
    double getLossPriceHat(size_t bidderIdx, SliceIdx objectSlice);
    double getPriceHat(const SliceIdx& objectSlice);
    void checkEpsilonCSS();
};
} // namespace auction_sop
#endif
