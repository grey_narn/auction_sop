#ifndef STAT_KEEPER_H
#define STAT_KEEPER_H

#include <string>
#include <fstream>

#include "basic_defs.h"

namespace auction_sop
{


struct StatKeeper {
    public:
        StatKeeper() {};
        StatKeeper(const std::string statFileName); 
        ~StatKeeper(); 

        int iterNum {0};
        int phaseNum {0};
        MassType unassignedMass {0};
        MassType unassignedDiagMass {0};
        int unassignedBidders {0};
        int unassignedDiagBidders{0};
        int totalNumSlices;
        int totalNumDiagSlices;
        int totalNumNormalSlices;
        int numSlicesErased;
        int numSlicesCreated;
        int priceHeapTraversal;
        int pushesToPriceHeap;
        int pushesToObjectBidHeap;
        int pushesToSliceHeap;
        int decreasePriceKey;

        void printStats();
        void printStats(const char* statFileName);
        void resetForPhase();
        void resetForIteration();
    private:
        std::ofstream output;
};

} // end of namespace auction_sop
#endif
