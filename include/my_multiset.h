#ifndef MY_MULTISET_H
#define MY_MULTISET_H

#include <iostream>
#include <map>
#include <unordered_map>

template<typename T>
class MyMultiset {
    private:
        size_t _weighted_size { 0 };
    public:

        MyMultiset() {}
        
        void insert(const T& element, size_t multiplicity)
        {
            auto findIter = myMap.find(element);
            if ( findIter == myMap.end() ) {
                myMap[element] = multiplicity;
            } else {
                findIter->second += multiplicity;
            }
            _weighted_size += multiplicity;
        }

        void erase(const T& element, size_t multiplicity) 
        {
            auto findIter = myMap.find(element);
            assert( findIter != myMap.end() );
            assert( findIter->second >= multiplicity );
            if ( findIter->second > multiplicity ) {
                findIter->second -= multiplicity;
            } else {
                myMap.erase(findIter);
            }
            _weighted_size -= multiplicity;
        }
 
        bool has_element(const T& element) const 
        {
            return myMap.count(element) == 1;
        }

        size_t multiplicity(const T& element) const
        {
            auto findIter = myMap.find(element);
            if (findIter == myMap.end())
                return 0;
            else {
                assert(findIter->second > 0);
                return findIter->second;
            }
        }

        size_t size(void) const
        {
            return myMap.size();
        }

        size_t weighted_size() const {
            return _weighted_size;
        }

        void clear(void)
        {
            myMap.clear();
        }

        bool empty(void) const
        {
            return size() == 0;
        }

        typedef typename std::unordered_map<T, size_t>::value_type PairType;
        typename std::unordered_map<T, size_t> myMap;
        typename std::unordered_map<T, size_t>::iterator begin() { return myMap.begin(); }
        typename std::unordered_map<T, size_t>::iterator end() { return myMap.end(); }
        typename std::unordered_map<T, size_t>::const_iterator cbegin() const { return myMap.cbegin(); }
        typename std::unordered_map<T, size_t>::const_iterator cend() const { return myMap.cend(); }

        friend std::ostream& operator<<(std::ostream& output, const MyMultiset& m){
            output << "{{ " << std::endl;
            for(const auto& p : m.myMap) {
                output << p.first << ": " << p.second << std::endl;
            }
            output << "}} " << std::endl;
            return output;
        };
};

template<typename T, typename C>
class MyOrderedMultiset {
    private:
        size_t _weighted_size { 0 };
    public:

        MyOrderedMultiset() {}
        
        void insert(const T& element, size_t multiplicity)
        {
            auto findIter = myMap.find(element);
            if ( findIter == myMap.end() ) {
                myMap[element] = multiplicity;
            } else {
                findIter->second += multiplicity;
            }
            _weighted_size += multiplicity;
        }

        void erase(const T& element, size_t multiplicity) 
        {
            auto findIter = myMap.find(element);
            assert( findIter != myMap.end() );
            assert( findIter->second >= multiplicity );
            if ( findIter->second > multiplicity ) {
                findIter->second -= multiplicity;
            } else {
                myMap.erase(findIter);
            }
            _weighted_size -= multiplicity;
        }
 
        bool has_element(const T& element) const 
        {
            return myMap.count(element) == 1;
        }

        size_t multiplicity(const T& element) const
        {
            auto findIter = myMap.find(element);
            if (findIter == myMap.end())
                return 0;
            else {
                assert(findIter->second > 0);
                return findIter->second;
            }
        }

        size_t size(void) const
        {
            return myMap.size();
        }

        size_t weighted_size() const {
            return _weighted_size;
        }

        void clear(void)
        {
            myMap.clear();
        }

        bool empty(void) const
        {
            return size() == 0;
        }

        typedef typename std::map<T, size_t, C>::value_type PairType;
        typename std::map<T, size_t, C> myMap;
        typename std::map<T, size_t, C>::iterator begin() { return myMap.begin(); }
        typename std::map<T, size_t, C>::iterator end() { return myMap.end(); }
        typename std::map<T, size_t, C>::const_iterator cbegin() const { return myMap.cbegin(); }
        typename std::map<T, size_t, C>::const_iterator cend() const { return myMap.cend(); }

        friend std::ostream& operator<<(std::ostream& output, const MyOrderedMultiset& m){
            output << "{{ " << std::endl;
            for(const auto& p : m.myMap) {
                output << p.first << ": " << p.second << std::endl;
            }
            output << "}} " << std::endl;
            return output;
        };
};


#endif
