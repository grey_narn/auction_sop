#ifndef AUCTION_KDTREE_H
#define AUCTION_KDTREE_H



#define USE_ARRAY_KEEPERS
#define GATHER_STAT_KDTREE


#include <list>
#include <vector>
#include <memory>
#include <fstream>
#include <unordered_set>
#include <unordered_map>
#include <functional>
#include <boost/heap/d_ary_heap.hpp>

#include "basic_defs.h"
#include "my_multiset.h"
#include "bid_table.h"
#include "diagonal_pool.h"

#include <dnn/geometry/euclidean-fixed.h>
#include <dnn/local/kd-tree.h>

namespace auction_sop
{


struct StatKeeper {
    public:
        StatKeeper() {};
        StatKeeper(const std::string statFileName); 
        ~StatKeeper(); 

        int iterNum {0};
        int phaseNum {0};
        MassType unassignedMass {0};
        int unassignedBidders;
        int totalNumSlices;
        int totalNumDiagSlices;
        int totalNumNormalSlices;
        int numSlicesErased;
        int numSlicesCreated;
        int priceHeapTraversal;
        int pushesToPriceHeap;
        int pushesToObjectBidHeap;
        int pushesToSliceHeap;
        int decreasePriceKey;
        MyMultiset<double> diagSlicePrices;

        void printStats();
        void printStats(const char* statFileName);
        void resetForPhase();
        void resetForIteration();
    private:
        std::ofstream output;
};

using BidderHeap = boost::heap::d_ary_heap<PointSlice, 
      boost::heap::arity<2>, boost::heap::mutable_<true>,
      boost::heap::compare<CompPointSlicesByPrice>>;
using BidderHeapHandle = BidderHeap::handle_type;

template<typename T>
class SliceInfoKeeperArray {
    public:
        SliceInfoKeeperArray(size_t _numBidders, size_t _numObjects, const T& _defaultValue, StatKeeper* sk = nullptr) :
            numBidders(_numBidders),
            numObjects(_numObjects),
            defaultValue(_defaultValue),
            assignedSliceCells(boost::extents[_numBidders][_numObjects],  boost::fortran_storage_order()),
            unassignedSliceCells(boost::extents[_numBidders][_numObjects], boost::fortran_storage_order()),
            statKeeper(sk)
        {
            assert(_numBidders > 0);
            assert(_numObjects > 0);
            for(size_t bidderIdx = 0; bidderIdx < numBidders; ++bidderIdx) {
                for(size_t objectIdx = 0; objectIdx < numObjects; ++objectIdx) {
                    assignedSliceCells[bidderIdx][objectIdx] = defaultValue;
                    unassignedSliceCells[bidderIdx][objectIdx] = defaultValue;
                }
            }
        }

        bool hasSlice(const SliceIdx& slice) 
        {
            return getValue(slice) != defaultValue;
        }

        inline T getValue(const SliceIdx& slice) 
        {
            if ( slice.getIsAssigned() ) {
                return assignedSliceCells[slice.getOwnerIdx()][slice.getObjectIdx()];
            } else {
                return unassignedSliceCells[slice.getOwnerIdx()][slice.getObjectIdx()];
            }
        }

        inline T getValue(const size_t ownerIdx, const size_t objectIdx, const bool isAssigned) 
        {
            if ( isAssigned ) {
                return assignedSliceCells[ownerIdx][objectIdx];
            } else {
                return unassignedSliceCells[ownerIdx][objectIdx];
            }
        }


        inline void setValue(const SliceIdx& slice, const T& newValue)
        {
            if ( slice.getIsAssigned() ) {
                assignedSliceCells[slice.getOwnerIdx()][slice.getObjectIdx()] = newValue;
            } else {
                unassignedSliceCells[slice.getOwnerIdx()][slice.getObjectIdx()] = newValue;
            }
        }

        void flushAssignment()
        {
            unassignedSliceCells = assignedSliceCells;
            //std::fill(assignedSliceCells.begin(), assignedSliceCells.end(), defaultValue);
            for(size_t bidderIdx = 0; bidderIdx < numBidders; ++bidderIdx) {
                for(size_t objectIdx = 0; objectIdx < numObjects; ++objectIdx) {
                    assignedSliceCells[bidderIdx][objectIdx] = defaultValue;
                }
            }
        }

        void erase(const SliceIdx& slice)
        {
            setValue(slice, defaultValue);
        }

    private:
        size_t numBidders;
        size_t numObjects;
        T defaultValue;
        boost::multi_array<T, 2> assignedSliceCells;
        boost::multi_array<T, 2> unassignedSliceCells;
        StatKeeper* statKeeper;
};

template<>
class SliceInfoKeeperArray<BidderHeapHandle> {
    public:
        SliceInfoKeeperArray(size_t _numBidders, size_t _numObjects, const BidderHeapHandle& _defaultValue, StatKeeper* sk = nullptr) :
            numBidders(_numBidders),
            numObjects(_numObjects),
            defaultValue(_defaultValue),
            assignedSliceCells(boost::extents[_numBidders][_numObjects], boost::fortran_storage_order()),
            unassignedSliceCells(boost::extents[_numBidders][_numObjects], boost::fortran_storage_order()),
            statKeeper(sk)
        {
            assert(_numBidders > 0);
            assert(_numObjects > 0);
            for(size_t bidderIdx = 0; bidderIdx < numBidders; ++bidderIdx) {
                for(size_t objectIdx = 0; objectIdx < numObjects; ++objectIdx) {
                    assignedSliceCells[bidderIdx][objectIdx] = defaultValue;
                    unassignedSliceCells[bidderIdx][objectIdx] = defaultValue;
                }
            }
        }

        bool hasSlice(const SliceIdx& slice) 
        {
            return getValue(slice) != defaultValue;
        };

        BidderHeapHandle getValue(const SliceIdx& slice)
        {
            if ( slice.getIsAssigned() ) {
                return assignedSliceCells[slice.getOwnerIdx()][slice.getObjectIdx()];
            } else {
                return unassignedSliceCells[slice.getOwnerIdx()][slice.getObjectIdx()];
            }
        }

        void setValue(const SliceIdx& slice, const BidderHeapHandle& newValue)
        {
            if ( slice.getIsAssigned() ) {
                assignedSliceCells[slice.getOwnerIdx()][slice.getObjectIdx()] = newValue;
            } else {
                unassignedSliceCells[slice.getOwnerIdx()][slice.getObjectIdx()] = newValue;
            }
        }

        void flushAssignment() = delete;

        void flushAssignment(BidderHeap& priceHeap)
        {
            BidderHeap oldHeap = priceHeap;
            priceHeap.clear();
            for(size_t bidderIdx = 0; bidderIdx < numBidders; ++bidderIdx) {
                for(size_t objectIdx = 0; objectIdx < numObjects; ++objectIdx) {
                    assignedSliceCells[bidderIdx][objectIdx] = defaultValue;
                    unassignedSliceCells[bidderIdx][objectIdx] = defaultValue;
                }
            }
            for(auto p : oldHeap) {
                p.sliceIdx.isAssigned = false;
                unassignedSliceCells[p.sliceIdx.getOwnerIdx()][p.sliceIdx.getObjectIdx()] = priceHeap.push(p);
#ifdef GATHER_STAT_KDTREE
                if (statKeeper != nullptr) {
                    statKeeper->pushesToPriceHeap += priceHeap.size();
                }
#endif
            }
        }

        void erase(const SliceIdx& slice)
        {
            setValue(slice, defaultValue);
        }

    private:
        size_t numBidders;
        size_t numObjects;
        BidderHeapHandle defaultValue;
        boost::multi_array<BidderHeapHandle, 2> assignedSliceCells;
        boost::multi_array<BidderHeapHandle, 2> unassignedSliceCells;
        StatKeeper* statKeeper;
};


template<typename T>
class SliceInfoKeeperMap {
    public:
        SliceInfoKeeperMap(size_t _numBidders, size_t _numObjects, const T& _defaultValue, StatKeeper* sk = nullptr) :
            numBidders(_numBidders),
            numObjects(_numObjects),
            defaultValue(_defaultValue),
            statKeeper(sk)
        {
            assert(_numBidders > 0);
            assert(_numObjects > 0);
        }

        bool hasSlice(const SliceIdx& slice) 
        {
            return _map.find(slice) != _map.end();
        };

        T getValue(const SliceIdx& slice)
        {
            auto mapIter = _map.find(slice);
            if (mapIter == _map.end())
                return defaultValue;
            else
                return mapIter->second;
        }

        void setValue(const SliceIdx& slice, const T& newValue)
        {
            auto mapIter = _map.find(slice);
            if (mapIter == _map.end()) {
                _map[slice] = newValue;
            } else {
                mapIter->second = newValue;
            }
        }

        void flushAssignment()
        {
            std::unordered_map<SliceIdx, T, SliceIdxHash> newMap;
            for(auto mapIter = _map.begin(); mapIter != _map.end(); ++mapIter) {
                SliceIdx sliceIdx = mapIter->first;
                T newValue = mapIter->second;
                assert( sliceIdx.getIsAssigned());
                sliceIdx.isAssigned = false;
                newMap[sliceIdx] = newValue;
                //newMap.insert( std::make_pair<SliceIdx, T>(sliceIdx, newValue));
            }
            _map = newMap;
        }

        void erase(const SliceIdx& slice)
        {
            assert(hasSlice(slice));
            _map.erase(slice);
        }
    private:
        size_t numBidders;
        size_t numObjects;
        T defaultValue;
        std::unordered_map<SliceIdx, T, SliceIdxHash> _map;
        StatKeeper* statKeeper; // not owned, should not be destroyed
};

template<>
class SliceInfoKeeperMap<BidderHeapHandle> {
    public:
        inline SliceInfoKeeperMap(size_t _numBidders, size_t _numObjects, const BidderHeapHandle& _defaultValue, StatKeeper* sk = nullptr) :
            numBidders(_numBidders),
            numObjects(_numObjects),
            defaultValue(_defaultValue),
            statKeeper(sk)
        {
            assert(_numBidders > 0);
            assert(_numObjects > 0);
        }

        inline bool hasSlice(const SliceIdx& slice) 
        {
            return _map.find(slice) != _map.end();
        };

        inline BidderHeapHandle getValue(const SliceIdx& slice)
        {
            auto mapIter = _map.find(slice);
            if (mapIter == _map.end())
                return defaultValue;
            else
                return mapIter->second;
        }

        inline void setValue(const SliceIdx& slice, const BidderHeapHandle& newValue)
        {
            auto mapIter = _map.find(slice);
            if (mapIter == _map.end()) {
                _map[slice] = newValue;
            } else {
                mapIter->second = newValue;
            }
        }

        inline void flushAssignment()
        {
            // do not call this method:
            // we need to make all assigned slices unassigned, 
            // so we need the priceHeap to do it
            assert(false);
        }

        inline void flushAssignment(BidderHeap& priceHeap)
        {
            BidderHeap oldHeap = priceHeap;
            priceHeap. clear();
            _map.clear();
            for(auto p : oldHeap) {
                assert(p.sliceIdx.isAssigned);
                p.sliceIdx.isAssigned = false;
                _map[p.sliceIdx] = priceHeap.push(p);
            }
#ifdef GATHER_STAT_KDTREE
            if (statKeeper != nullptr) {
                statKeeper->pushesToPriceHeap += _map.size();
            }
#endif
        }


        inline void erase(const SliceIdx& slice)
        {
            assert(hasSlice(slice));
            _map.erase(slice);
        }

    private:
        size_t numBidders;
        size_t numObjects;
        BidderHeapHandle defaultValue;
        std::unordered_map<SliceIdx, BidderHeapHandle, SliceIdxHash> _map;
        StatKeeper* statKeeper;
};




//using BidderHeapHandleKeeper = BidderHeapHandleKeeperArray;
//using MassKeeper = SliceInfoKeeperArray<MassType>;
//using PriceKeeper = SliceInfoKeeperArray<double>;

#ifdef USE_ARRAY_KEEPERS
using BidderHeapHandleKeeper = SliceInfoKeeperArray<BidderHeapHandle>;
using MassKeeper = SliceInfoKeeperArray<MassType>;
using PriceKeeper = SliceInfoKeeperArray<double>;
#else
using BidderHeapHandleKeeper = SliceInfoKeeperMap<BidderHeapHandle>;
using MassKeeper = SliceInfoKeeperMap<MassType>;
using PriceKeeper = SliceInfoKeeperMap<double>;
#endif



class AuctionKdtreeRunner{
public:


    friend class DiagonalPoolHeap;
    friend class DiagonalPool;

    typedef dnn::Point<2, double> DnnPoint;
    typedef dnn::PointTraits<DnnPoint> DnnTraits;
    typedef DnnTraits::PointHandle DnnPointHandle;

    AuctionKdtreeRunner(std::vector<DiagramPoint>& A,
            std::vector<DiagramPoint>& ADiag,
            std::vector<DiagramPoint>& B,
            std::vector<DiagramPoint>& BDiag,
            double _wassersteinPower,
            double _delta, 
            const std::string fnameStat = std::string(""));
    ~AuctionKdtreeRunner() {  }
    // types
    using RealMatrix = boost::multi_array<double, 2>;
    //using MassMatrix = boost::multi_array<MassType, 2>;
    //using Time = long int;
    //using SliceTimePair = std::pair<SliceIdx, Time>;
    //using UpdateList = std::list<SliceTimePair>;
    //using UpdateListIter = UpdateList::iterator;

    // data members
    // for statistics, should be initialized first
    StatKeeper statKeeper;
    
    dnn::KDTree<DnnTraits>* kdtree;
    dnn::KDTree<DnnTraits>* kdtreeDiag;
    size_t numNormalBidders;
    size_t numDiagBidders;
    size_t numBidders;
    size_t numNormalObjects;
    size_t numDiagObjects;
    size_t numObjects;
    MassType totalMass { 0 };
    //RealMatrix valueMatrix;

    double epsilon;
    double wassersteinPower;
    double maxEpsilon;
    double maxVal;
    double minPersistence;
    double minPossibleEpsilon { 0.0 };
    int maxIterNum { 20 };
    double initialEpsToMaxValFactor { 6.0 };

    // additional data structures to get fast access to unassigned bidders and
    // objects with bids
    // maps bidder to the number of objects assigned to this bidder; if it gets
    // 0, the key is deleted 
    MyMultiset<MassType> unassignedBidders;
    std::unordered_set<size_t> submitters;
    std::unordered_set<size_t> objectsWithBids;
    std::unordered_set<size_t> poolsWithBids;
    std::vector<std::unordered_set<SliceIdx, SliceIdxHash>> objectSlicesWithBids;
    std::vector<std::unordered_set<SliceIdx, SliceIdxHash>> slicesAssignedToBidder;
    // heap-related members, to get best objects fast
    BidderHeap dummyHeap;
    BidderHeapHandle dummyHandle;
    BidderHeap priceHeap;
    DiagonalPoolHeap diagonalPoolHeap;
    //std::unordered_map<bool, std::unordered_map< size_t, std::unordered_map<size_t, BidderHeapHandle> > > priceHeapHandles;
    //boost::multi_array<BidderHeapHandle, 3> priceHeapHandles;
    //UpdateList updateList;
    //std::vector<Time> biddersUpdateMoments;
    //size_t timeCounter;
    
    double wassersteinDistance;
    double epsilonCommonRatio { 6.0 };
    double maxDiagToDiagDistance;
    double diagDistScalingFactor;
    //std::unordered_map<SliceIdx, MassType, SliceIdxHash> massMap;
    //std::unordered_map<SliceIdx, double, SliceIdxHash> priceMap;
    MassKeeper massKeeper;
    PriceKeeper priceKeeper;
    BidTableKeeper sliceBidTables;
    //std::unordered_map<SliceIdx, std::unique_ptr<ObjectBidTable>, SliceIdxHash> sliceBidTables;
    
    std::vector<DiagramPoint> allBidders, allObjects;
    std::vector<DnnPoint> dnnPoints; // for objects
    std::vector<DnnPoint> dnnDiagPoints; // for objects
    std::vector<DnnPointHandle> dnnPointHandles;
    std::vector<DnnPointHandle> dnnDiagPointHandles;
    std::vector<DnnPoint> dnnBidders; 
    std::vector<DnnPoint> dnnScaledDiagBidders; 
    std::vector<PointSliceContainer> objectSlices;
    std::vector<DiagonalPool> diagPools;
    //DiagonalPool diagPool;
    // methods
    // initialization routines
    void initKeepers();
    void initKDTree();
    void initKDTreeDiag();
    void initMatrices();
    void initHeap();
    // basic methods
    double getWassersteinDistance();
    void runAuction();
    void runAuctionPhase();
    void reassignFlow();
    void reassignSlice(const SliceIdx& slice, PointSliceHeap& newPointSlices, SliceIdxSet& deleteFromSlicesAssignedToBidder, SliceIdxSet& insertIntoSlicesAssignedToBidder);
    void reassignSlice(const SliceIdx& slice, PointSliceHeap& newPointSlices);
    void submitBids(MyMultiset<MassType>::PairType unassignedPair);
    void flushAssignment();
    // helper routines
    double getDistanceToQthPowerInternal();
    MassType getSliceMass(const SliceIdx& slice);
    double getSlicePrice(const SliceIdx& slice);
    void setSlicePrice(const PointSlice& newPointSlice);
    void setSliceMass(const SliceIdx& slice, const MassType newMass);
    void eraseSlice(const SliceIdx& slice);
    void submitBidToSlice(const SliceIdx& slice, const ObjectBid& bid);
    void submitBidToPool(const SliceIdx& slice, const ObjectBid& bid);
    void submitBidsForAssignedObjects(size_t bidderIdx, double w);
    void addBidToProjectionPreimage(size_t bidderIdx, MassType unassignedMass, BidTable& bidTable);
    double getDistToBidder(const size_t bidderIdx, const size_t objectIdx) const;
    // for debug
    void sanityCheck();
    friend std::ostream& operator<<(std::ostream& output, AuctionKdtreeRunner&);
    //void printBidHeap(std::ostream& output, size_t bidderIdx);
    double getErrorBound();
    double getLowerBound();
    double getMinEpsilon() const;

    double getLossPriceHat(size_t bidderIdx, SliceIdx objectSlice);
    double getPriceHat(SliceIdx objectSlice);
    void checkEpsilonCSS();
};
} // namespace auction_sop
#endif
