#ifndef SLICE_TABLE_H 
#define SLICE_TABLE_H 

#include <vector>
#include <set>
#include <assert.h>
#include <limits>
#include <unordered_set>

#include "my_multiset.h"

namespace auction_sop
{

using MassType = int;

struct SliceIdx {
    size_t ownerIdx;
    size_t objectIdx;
    bool isAssigned;
    SliceIdx(size_t _ownerIdx, size_t _objectIdx, bool _isAssigned) :
        ownerIdx(_ownerIdx), 
        objectIdx(_objectIdx),
        isAssigned(_isAssigned)
    {};
    SliceIdx() :
        ownerIdx(std::numeric_limits<size_t>::max()),
        objectIdx(std::numeric_limits<size_t>::max()),
        isAssigned(false)
    { };
    //SliceIdx(const SliceIdx& other) = default;
    bool operator==(const SliceIdx& other) const { return ownerIdx == other.ownerIdx and objectIdx ==  other.objectIdx and isAssigned == other.isAssigned; }
    bool operator!=(const SliceIdx& other) const { return not (*this == other); }
    size_t getOwnerIdx() const { return ownerIdx; }
    void setOwnerIdx(size_t newOwnerIdx) { ownerIdx = newOwnerIdx; }
    size_t getObjectIdx() const { return objectIdx; }
    void setObjectIdx(size_t newObjectIdx) { objectIdx = newObjectIdx; }
    bool getIsAssigned() const { return isAssigned; }
    void setIsAssigned(bool newVal) { isAssigned = newVal; }
};

struct SliceIdxHash{
    size_t operator()(const SliceIdx& s) const{
        return std::hash<size_t>()(s.ownerIdx) ^ std::hash<size_t>()(s.objectIdx) ^ std::hash<bool>()(s.isAssigned);
    }
};

struct PointSlice 
{
    SliceIdx sliceIdx;
    mutable MassType mass;
    double price;
    double persistence;
    PointSlice(const SliceIdx& _sliceIdx, MassType _mass, double _price, double _persistence) : 
        sliceIdx(_sliceIdx),
        mass(_mass), 
        price(_price),
        persistence(_persistence)
    { assert(mass>0); }
    PointSlice() :
        sliceIdx(SliceIdx()),
        mass(std::numeric_limits<MassType>::max()),
        price(std::numeric_limits<double>::max()),
        persistence(0.0)
    {
    }
    size_t getOwnerIdx() const { return sliceIdx.ownerIdx; }
    size_t getObjectIdx() const { return sliceIdx.objectIdx; }
    bool getIsAssigned() const { return sliceIdx.isAssigned; }
    void setIsAssigned(bool newVal) { sliceIdx.setIsAssigned(newVal); }
    SliceIdx getSliceIdx() const { return sliceIdx; }
    MassType getMass() const { return mass; }
    double getPrice() const { return price; }
    double getLossForDiagonal() const { return price + persistence; }
    // const is fine: mass is declared as mutable!
    void setMass(MassType newMass) const { assert(newMass > 0); mass = newMass; }
    void setPrice(double newPrice) { assert(newPrice >= price); price = newPrice; }
    void setOwnerIdx(size_t newOwnerIdx) { sliceIdx.setOwnerIdx(newOwnerIdx); }
    void setObjectIdx(size_t newObjectIdx) { sliceIdx.setObjectIdx(newObjectIdx); }

    void increaseMassBy(MassType delta) const {  mass += delta; assert(mass > 0); }
    bool operator==(const PointSlice& other) const { return sliceIdx == other.sliceIdx and mass ==  other.mass and price == other.price; }
    bool operator!=(const PointSlice& other) const { return not (*this == other); }
    //PointSlice(const& PointSlice other) = default;
};


struct PointSliceHash{
    size_t operator()(const PointSlice& s) const{
        return std::hash<size_t>()(s.sliceIdx.ownerIdx) ^ std::hash<size_t>()(s.sliceIdx.objectIdx) ^ std::hash<bool>()(s.sliceIdx.isAssigned) ^ std::hash<double>()(s.price);
    }
};


struct CompPointSlicesByPrice {
    bool operator()(const PointSlice& s1, const PointSlice& s2) const {
        return s1.price > s2.price;
    }
};

struct CompPointSlicesByPriceLess {
    bool operator()(const PointSlice& s1, const PointSlice& s2) const {
        return s1.price < s2.price;
    }
};


using PointSliceContainer = std::vector<PointSlice>;

struct PointSliceComparator{
    bool operator()(const PointSlice& s1, const PointSlice& s2) const {
        //assert(s1.getIsAssigned() and s2.getIsAssigned());
        return (s1.price > s2.price) or 
            (s1.price == s2.price and s1.getOwnerIdx() > s2.getOwnerIdx() ) or
            (s1.price == s2.price and s1.getOwnerIdx() == s2.getOwnerIdx() and s1.getIsAssigned() and not s2.getIsAssigned() );
   }
};

struct PointSliceComparatorAsc{
    bool operator()(const PointSlice& s1, const PointSlice& s2) const {
        //assert(s1.getIsAssigned() and s2.getIsAssigned());
        return (s1.price < s2.price) or 
               (s1.price == s2.price and not s1.getIsAssigned() and s2.getIsAssigned() ) or
               (s1.price == s2.price and s1.getIsAssigned() == s2.getIsAssigned() and s1.getOwnerIdx() < s2.getOwnerIdx() ) or
               (s1.price == s2.price and s1.getIsAssigned() == s2.getIsAssigned() and s1.getOwnerIdx() == s2.getOwnerIdx() and s1.getObjectIdx() < s2.getObjectIdx());
    }
};

struct PointSliceComparatorAscPers{
    bool operator()(const PointSlice& s1, const PointSlice& s2) const {
        //assert(s1.getIsAssigned() and s2.getIsAssigned());
        return (s1.getLossForDiagonal() < s2.getLossForDiagonal()) or
               (s1.getLossForDiagonal() == s2.getLossForDiagonal() and s1.price  < s2.price) or 
               (s1.getLossForDiagonal() == s2.getLossForDiagonal() and s1.price == s2.price and not s1.getIsAssigned() and s2.getIsAssigned() ) or
               (s1.getLossForDiagonal() == s2.getLossForDiagonal() and s1.price == s2.price and s1.getIsAssigned() == s2.getIsAssigned() and s1.getOwnerIdx() < s2.getOwnerIdx() ) or
               (s1.getLossForDiagonal() == s2.getLossForDiagonal() and s1.price == s2.price and s1.getIsAssigned() == s2.getIsAssigned() and s1.getOwnerIdx() == s2.getOwnerIdx() and s1.getObjectIdx() < s2.getObjectIdx());
    }
};

//using PointSliceHeap = boost::heap::d_ary_heap<PointSlice,
      //boost::heap::arity<2>, boost::heap::mutable_<true>,
      //boost::heap::compare<CompAssignedPointSlicesByPriceAndOwner>>;


using PointSliceHeap = std::set<PointSlice, PointSliceComparator>;
using PointSliceHeapAsc = std::set<PointSlice, PointSliceComparatorAsc>;
using SliceIdxSet = std::unordered_set<SliceIdx, SliceIdxHash>;

using PointSliceHeapDiag = std::set<PointSlice, PointSliceComparatorAscPers>;

std::ostream& operator<<(std::ostream& output, const SliceIdx& s);
std::ostream& operator<<(std::ostream& output, const PointSlice& s);
std::ostream& operator<<(std::ostream& output, const PointSliceHeapAsc& s);
std::ostream& operator<<(std::ostream& output, const PointSliceHeapDiag& ps);

} // namespace 
#endif
