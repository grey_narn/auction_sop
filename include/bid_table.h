#ifndef BID_TABLE_H
#define BID_TABLE_H

#include <vector>
#include <assert.h>
#include <unordered_set>
#include <queue>
#include <boost/multi_array.hpp>
#include <boost/heap/d_ary_heap.hpp>

#include "my_multiset.h"
#include "slice_table.h"

namespace auction_sop
{
// to-do: add slice idx or pointer to slice?
//

using MassType = int;

struct Bid {
    size_t bidderIdx;
    PointSlice slice;
    mutable MassType mass;
    double loss;
    MassType availableMass;
    Bid(size_t _bidderIdx, const PointSlice& _slice, MassType _mass, double _loss, MassType _availableMass) :
        bidderIdx(_bidderIdx),
        slice(_slice),
        mass(_mass),
        loss(_loss),
        availableMass(_availableMass)
    {
        assert( not slice.getIsAssigned() or bidderIdx != slice.getOwnerIdx());
        assert(availableMass >= mass);
    };
    bool thereAreMoreObjects() const { return availableMass > mass; }
};

bool compareBidsByloss(const Bid& b1, const Bid& b2);

using BidList = std::vector<Bid>;
using BidListIter = BidList::iterator;

struct StupidBidTable{
    // fields
    MassType capacity;
    MassType totalMass { 0 };
    BidList bidList;
    double w;
    std::unordered_set<size_t> objectSet;
    // methods
    StupidBidTable(const MassType _capacity, std::unordered_set<size_t> _objSet) : capacity(_capacity), objectSet(_objSet) {};
    void push_back(const Bid& bid);
    void sanityCheck() const;
    bool isFull() const;
    double getW() const { return w; }
    BidListIter begin() { return bidList.begin(); };
    BidListIter end() { return bidList.end(); };
    Bid& back() { return bidList.back(); }
    bool thereAreMoreObjects() { return cutLastEntry or bidList.back().thereAreMoreObjects(); }
    // for debug
    friend std::ostream& operator<<(std::ostream& output, const StupidBidTable& bt);
    std::vector<double> getDebuglosss();
private:
    bool cutLastEntry { false };
};

struct CompBidsByloss {
    bool operator()(const Bid& b1, const Bid& b2) const {
        return b1.loss < b2.loss or
               (b1.loss == b2.loss and not b1.slice.getIsAssigned() and b2.slice.getIsAssigned()) or
               (b1.loss == b2.loss and b1.slice.getIsAssigned() == b2.slice.getIsAssigned() and b1.slice.getMass() > b2.slice.getMass());
    }
};

struct CompBidsBylossGreater {
    bool operator()(const Bid& b1, const Bid& b2) const {
        return b1.loss > b2.loss or
               (b1.loss == b2.loss and not b1.slice.getIsAssigned() and b2.slice.getIsAssigned()) or
               (b1.loss == b2.loss and b1.slice.getIsAssigned() == b2.slice.getIsAssigned() and b1.slice.getMass() > b2.slice.getMass());
    }
};


//using BidHeap = boost::heap::d_ary_heap<Bid,
      //boost::heap::arity<2>, boost::heap::mutable_<false>,
      //boost::heap::compare<CompBidsByloss>>;

class BidHeap {
    public:
        typedef std::vector<Bid> BidContainer;
        typedef BidContainer::iterator iterator;
        Bid& top() { return container.front(); }
        void push(const Bid& bid);
        void push(size_t bidderIdx, const PointSlice& slice, MassType mass, double loss, MassType availableMass);
        void pop();
        size_t size() const { return container.size(); }
        bool empty() const { return container.empty(); }
        iterator begin() { return container.begin(); }
        iterator end() { return container.end(); }
        iterator ordered_begin() { return container.begin(); }
        iterator ordered_end() { return container.end(); }
        BidHeap() {}
        BidHeap(const MassType initialCapacity) { container.reserve(initialCapacity); }
        BidContainer container;
        CompBidsByloss comp;
};

using BidHeapIter = BidHeap::iterator;

struct BidTable{
    // fields
    MassType capacity;
    MyMultiset<size_t> objectSet;
    BidHeap bidHeap;
    BidHeap wHeap;
    MassType bidHeapAccumulatedMass { 0 };
    MassType wHeapAccumulatedMass { 0 };
    // methods
    BidTable(const MassType _capacity, MyMultiset<size_t> _objSet) : capacity(_capacity), objectSet(_objSet) {};
    void insert(const Bid& bid);
    void insert(const size_t bidderIdx, const PointSlice& slice, const MassType bidMass, const double sliceProfit, const MassType availableMass);
    void sanityCheck() const;
    bool isFull() const;
    double getW() const;
    size_t numberOfBids() const { return bidHeap.size(); }
    BidHeapIter begin() { return bidHeap.begin(); };
    BidHeapIter end() { return bidHeap.end(); };
    friend std::ostream& operator<<(std::ostream& output, const BidTable& bt);
    bool thereAreMoreObjects() { return cutLastEntry or bidHeap.top().thereAreMoreObjects(); }
    // for debug
    std::vector<double> getDebuglosss();
private:
    bool cutLastEntry { false };
};

struct ObjectBid {
    size_t bidderIdx;
    mutable MassType mass;
    double value;
    ObjectBid(size_t _bidderIdx, MassType _mass, double _value) : bidderIdx(_bidderIdx), mass(_mass), value(_value) {};
};

struct CompObjectBidsByValue{
    bool operator()(const ObjectBid& b1, const ObjectBid& b2) const {
        return b1.value > b2.value;
    }
};

using ObjectBidHeap = boost::heap::d_ary_heap<ObjectBid,
      boost::heap::arity<2>, boost::heap::mutable_<false>,
      boost::heap::compare<CompObjectBidsByValue>>;

using ObjectBidHeapIter = ObjectBidHeap::iterator;

struct ObjectBidTable {
    ObjectBidTable(MassType _capacity) :
        capacity(_capacity)
    {};
    MassType capacity;
    MassType totalMass { 0 };
    ObjectBidHeap bidHeap;
    ObjectBidHeapIter begin() { return bidHeap.begin(); }
    ObjectBidHeapIter end() { return bidHeap.end(); }
    size_t size() const { return bidHeap.size(); }
    void insert(const ObjectBid& bid);
    void clear();
    void sanityCheck() const;
    bool isFull() { return totalMass >= capacity; }
};

class BidTableKeeperArray {
public:
    BidTableKeeperArray(size_t numBidders, size_t numObjects);
    ~BidTableKeeperArray();
    ObjectBidTable* getSliceBidTable(const SliceIdx& sliceIdx);
    void setCapacity(const SliceIdx& slice, MassType newCapacity);
    void submitBid(const SliceIdx& slice, const ObjectBid& bid);
private:
    size_t numBidders;
    size_t numObjects;
    boost::multi_array<ObjectBidTable*, 2> assignedObjectsBidTables;
    boost::multi_array<ObjectBidTable*, 2> unassignedObjectsBidTables;
};

using BidTableKeeper = BidTableKeeperArray;


std::ostream& operator<<(std::ostream& output, const Bid& bid);
std::ostream& operator<<(std::ostream& output, BidTable& bidTable);
std::ostream& operator<<(std::ostream& output, const ObjectBid& bid);
std::ostream& operator<<(std::ostream& output, const ObjectBidTable& bidTable);
std::ostream& operator<<(std::ostream& output, BidHeap& h);

}

#endif
