#ifndef DNN_LOCAL_SEARCH_FUNCTORS_H
#define DNN_LOCAL_SEARCH_FUNCTORS_H

#include <boost/range/algorithm/heap_algorithm.hpp>
#include "my_multiset.h"
#include "bid_table.h"

using auction_sop::BidTable;
using auction_sop::Bid;
using auction_sop::PointSliceContainer;
using auction_sop::PointSliceHeapAsc;
using auction_sop::MassType;

namespace dnn
{

template<class NN>
struct HandleDistance
{
    typedef             typename NN::PointHandle                                    PointHandle;
    typedef             typename NN::DistanceType                                   DistanceType;
    typedef             typename NN::HDContainer                                    HDContainer;

                        HandleDistance()                                            {}
                        HandleDistance(PointHandle pp, DistanceType dd):
                            p(pp), d(dd)                                            {}
    bool                operator<(const HandleDistance& other) const                { return d < other.d; }

    PointHandle         p;
    DistanceType        d;
};

template<class HandleDistance>
struct NNRecord
{
    typedef         typename HandleDistance::PointHandle                            PointHandle;
    typedef         typename HandleDistance::DistanceType                           DistanceType;

                    NNRecord()                                                      { result.d = std::numeric_limits<DistanceType>::infinity(); }
    DistanceType    operator()(PointHandle p, DistanceType d)                       { if (d < result.d) { result.p = p; result.d = d; } return result.d; }
    HandleDistance  result;
};

template<class HandleDistance>
struct rNNRecord
{
    typedef         typename HandleDistance::PointHandle                            PointHandle;
    typedef         typename HandleDistance::DistanceType                           DistanceType;
    typedef         typename HandleDistance::HDContainer                            HDContainer;

                    rNNRecord(DistanceType r_): r(r_)                               {}
    DistanceType    operator()(PointHandle p, DistanceType d)
    {
        if (d <= r)
            result.push_back(HandleDistance(p,d));
        return r;
    }

    DistanceType    r;
    HDContainer     result;
};

template<class HandleDistance>
struct kNNRecord
{
    typedef         typename HandleDistance::PointHandle                            PointHandle;
    typedef         typename HandleDistance::DistanceType                           DistanceType;
    typedef         typename HandleDistance::HDContainer                            HDContainer;

                    kNNRecord(unsigned k_): k(k_)                                   {}
    DistanceType    operator()(PointHandle p, DistanceType d)
    {
        if (result.size() < k)
        {
            result.push_back(HandleDistance(p,d));
            boost::push_heap(result);
            if (result.size() < k)
                return std::numeric_limits<DistanceType>::infinity();
        } else if (d < result[0].d)
        {
            boost::pop_heap(result);
            result.back() = HandleDistance(p,d);
            boost::push_heap(result);
        }
        if ( result.size() > 1 ) {
            assert( result[0].d >= result[1].d );
        }
        return result[0].d;
    }

    unsigned        k;
    HDContainer     result;
};

// functor for AuctionSOP
template<class HandleDistance>
struct sopWRecord{

    typedef         typename HandleDistance::PointHandle                            PointHandle;
    typedef         typename HandleDistance::DistanceType                           DistanceType;
    typedef         typename HandleDistance::HDContainer                            HDContainer;

    // data members

    PointHandle queryPointHandle;
    double wassersteinPower;
    size_t bidderIdx;
    BidTable* bidTable;
    std::vector<PointSliceHeapAsc>* pointSlices;
    // methods
    sopWRecord(PointHandle _queryPointHandle,  double  _wassersteinPower, BidTable* _bidTable, std::vector<PointSliceHeapAsc>* _pointSlices) : 
        queryPointHandle(_queryPointHandle),
        wassersteinPower(_wassersteinPower),
        bidderIdx(_queryPointHandle->id()),
        bidTable(_bidTable),
        pointSlices(_pointSlices)
    {
    };

    DistanceType operator()(PointHandle p, DistanceType d)
    {
        size_t objectIdx = p->id();
        //std::cout << "in functor, objectIdx = " << objectIdx << std::endl;
        //DistanceType returnValue = bidTable->getW(); 
        DistanceType distToPoint = ( wassersteinPower > 1.0 ) ? pow(queryPointHandle->distance(*p), wassersteinPower) : queryPointHandle->distance(*p);
        for(const auto& slice : (*pointSlices)[objectIdx] ) {
            // do not bid for objects assigned to the same bidder
            if ( slice.getIsAssigned() and slice.getOwnerIdx() == bidderIdx) {
                continue;
            }
            DistanceType sliceProfit = distToPoint + slice.price;
            // the slice has no chance: stop traversing slices
            if (bidTable->isFull() and sliceProfit >= bidTable->getW() ) {
                break;
            }
            //  insert new bid into heap
            MassType availableMass = slice.mass;
            MassType bidMass = std::min(bidTable->capacity, availableMass);
            Bid newBid(bidderIdx, slice, bidMass, sliceProfit, availableMass);
            bidTable->insert(newBid);
            //bidTable->insert(bidderIdx, slice, bidMass, sliceProfit, availableMass);
        } // loop over PointSlices
        return bidTable->getW();
    }
};

// functor for AuctionSOP
template<class HandleDistance>
struct sopWDiagRecord{

    typedef         typename HandleDistance::PointHandle                            PointHandle;
    typedef         typename HandleDistance::DistanceType                           DistanceType;
    typedef         typename HandleDistance::HDContainer                            HDContainer;

    // data members

    PointHandle queryPointHandle;
    double wassersteinPower;
    size_t bidderIdx;
    size_t numDiagObjects;
    BidTable* bidTable;
    std::vector<PointSliceHeapAsc>* pointSlices;
    // methods
    sopWDiagRecord(PointHandle _queryPointHandle,  double  _wassersteinPower, size_t _numDiagObjects, BidTable* _bidTable, std::vector<PointSliceHeapAsc>* _pointSlices) : 
        queryPointHandle(_queryPointHandle),
        wassersteinPower(_wassersteinPower),
        bidderIdx(_queryPointHandle->id()),
        numDiagObjects(_numDiagObjects),
        bidTable(_bidTable),
        pointSlices(_pointSlices)
    {
        // diagonal objects == normal bidders
        assert(bidderIdx >= numDiagObjects);
        assert(numDiagObjects > 0);
    }

    DistanceType operator()(PointHandle p, DistanceType d)
    {
        size_t objectIdx = p->id();
        DistanceType distToPoint = pow( queryPointHandle->distance(*p), wassersteinPower);
        for(auto& slice : (*pointSlices)[objectIdx] ) {
            // do not bid for objects assigned to the same bidder
            if ( slice.getIsAssigned() and slice.getOwnerIdx() == bidderIdx) {
                continue;
            }
            DistanceType sliceProfit = distToPoint + slice.price;
            // the slice has no chance: stop traversing slices
            if (bidTable->isFull() and sliceProfit >= bidTable->getW() ) {
                break;
            }
            // process slice for wHeap:
            // 1. insert new bid into heap
            MassType availableMass = slice.mass;
            MassType bidMass = std::min(bidTable->capacity, availableMass);
            //Bid newBid(bidderIdx, slice, bidMass, sliceProfit, availableMass);
            //bidTable->insert(newBid);
            bidTable->insert(bidderIdx, slice, bidMass, sliceProfit, availableMass);
        } // loop over PointSlices
        return bidTable->getW();
    }
};

} // end of namespace dnn

#endif // DNN_LOCAL_SEARCH_FUNCTORS_H
