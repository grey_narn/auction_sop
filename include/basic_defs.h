#ifndef BASIC_DEFS_H
#define BASIC_DEFS_H

#include <vector>
#include <math.h>
#include <cstddef>
#include <unordered_map>
#include <unordered_set>
#include <iostream>
#include <string>
#include <assert.h>
#include <boost/functional/hash.hpp>

#include "def_debug.h"

#define MIN_VALID_ID 10

namespace auction_sop {

using MassType = int;
using PairVector = std::vector<std::pair<double, double>>;

struct Point {
    double x, y;
    bool operator==(const Point& other) const;
    bool operator!=(const Point& other) const;
    Point(double ax, double ay) : x(ax), y(ay) {}
    Point() : x(0.0), y(0.0) {}
    Point(const std::pair<double, double> p) : x(p.first), y(p.second) {}
    friend std::ostream& operator<<(std::ostream& output, const Point p);
};


struct PointHash {
    std::size_t operator()(const Point& p) const
    {
        size_t result = 0;
        boost::hash_combine(result, p.x);
        boost::hash_combine(result, p.y);
        return result;
    }
};

struct WeightedPoint {
    double x, y;
    MassType w;
    bool operator==(const WeightedPoint& other) const;
    bool operator!=(const WeightedPoint& other) const;
    friend std::ostream& operator<<(std::ostream& output, const WeightedPoint p);
    WeightedPoint(double _x, double _y, MassType _w) : x(_x), y(_y), w(_w) {}
    WeightedPoint() : x(0.0), y(0.0), w(0) {}
};

struct DiagramPoint
{
    // Points above the diagonal have type NORMAL
    // Projections onto the diagonal have type DIAG
    enum Type { NORMAL, DIAG};
    // data members
private:
    double x, y;
    MassType mass;
    Type type;
public:
    // operators, constructors
    bool operator==(const DiagramPoint& other) const;
    bool operator!=(const DiagramPoint& other) const;
    DiagramPoint(double xx, double yy, MassType w, Type ttype);
    DiagramPoint(const Point&, MassType);
    bool isDiagonal() const { return type == DIAG; }
    bool isNormal() const { return type == NORMAL; }
    Point getOrdinaryPoint() const; // for diagonal points return the coords or projection
    double getRealX() const; // return the x-coord
    double getRealY() const; // return the y-coord
    double getPersistence() const; // return the point persistence (y -x) / 2
    MassType getMass() const { return mass; }
    friend std::ostream& operator<<(std::ostream& output, const DiagramPoint p);
};

struct WeightedPointHash {
    size_t operator()(const WeightedPoint& p) const
    {
        size_t result = 0;
        boost::hash_combine(result, p.x);
        boost::hash_combine(result, p.y);
        boost::hash_combine(result, p.w);
        return result;
    }
};


struct DiagramPointHash {
    size_t operator()(const DiagramPoint& p) const
    {
        size_t result;
        boost::hash_combine(result, p.getRealX());
        boost::hash_combine(result, p.getRealY());
        boost::hash_combine(result, p.getMass());
        boost::hash_combine(result, p.isDiagonal());
        return result;
    }
};

double sqrDist(const Point& a, const Point& b);
double dist(const Point& a, const Point& b);
double distLInf(const Point& a, const Point& b);
double distLInf(const DiagramPoint& a, const DiagramPoint& b);

typedef std::unordered_set<Point, PointHash> PointSet;

class DiagramPointSet {
public:
    void insert(const DiagramPoint p);
    void erase(const DiagramPoint& p, bool doCheck = true); // if doCheck, erasing non-existing elements causes assert
    void erase(const std::unordered_set<DiagramPoint, DiagramPointHash>::const_iterator it);
    size_t size() const;
    void reserve(const size_t newSize);
    void clear();
    bool empty() const;
    bool hasElement(const DiagramPoint& p) const;
    std::unordered_set<DiagramPoint, DiagramPointHash>::iterator find(const DiagramPoint& p) { return points.find(p); };
    std::unordered_set<DiagramPoint, DiagramPointHash>::const_iterator find(const DiagramPoint& p) const { return points.find(p); };
    std::unordered_set<DiagramPoint, DiagramPointHash>::iterator begin() { return points.begin(); };
    std::unordered_set<DiagramPoint, DiagramPointHash>::iterator end() { return points.end(); }
    std::unordered_set<DiagramPoint, DiagramPointHash>::const_iterator cbegin() const { return points.cbegin(); }
    std::unordered_set<DiagramPoint, DiagramPointHash>::const_iterator cend() const { return points.cend(); }
    friend std::ostream& operator<<(std::ostream& output, const DiagramPointSet& ps);
private:
    std::unordered_set<DiagramPoint, DiagramPointHash> points;
};

// read diagram from file
bool readDiagramPointSet(const std::string& fname, std::vector<Point>& result);
bool readDiagramPointSet(const char* fname, std::vector<Point>& result);

// transform non-weighted points to weighted
void transform_to_diagrams(const std::vector<Point>& input_points_A,
                           const std::vector<Point>& input_points_B,
                           std::vector<DiagramPoint>& dgm_A,
                           std::vector<DiagramPoint>& dgm_B,
                           bool remove_duplicates = false);

// round points of dgm_in to the grid size, sum up weights, return result in
// dgm_out
void snap_diagram_to_grid(const std::vector<Point>& dgm_in,
                          const double grid_size,
                          std::vector<DiagramPoint>& dgm_out_norm,
                          std::vector<DiagramPoint>& dgm_out_diag,
                          double& err_estimate);

// round points of dgm_in to the grid size, sum up weights, return result in
// dgm_out
void snapDiagramToDiamondGrid(const std::vector<Point>& dgmIn,
                              const double gridSize,
                              std::vector<DiagramPoint>& dgmOutNorm,
                              std::vector<DiagramPoint>& dgmOutDiag);


void snapDiagramToDiamondGrid(const std::vector<Point>& dgmIn,
                              const double gridSize,
                              std::vector<DiagramPoint>& dgmOutNormal,
                              std::vector<DiagramPoint>& dgmOutDiagonal,
                              std::unordered_map<Point, std::vector<Point>, PointHash>& backInfo,
                              std::unordered_set<Point, PointHash>& discardedPoints);


template<typename DiagPointContainer>
double getFurthestDistance3Approx(DiagPointContainer& A, DiagPointContainer& B)
{
    double result { 0.0 };
    DiagramPoint currMinA = *(A.begin());
    for(auto minAIter = A.begin(); minAIter != A.end(); ++minAIter) {
        if ( minAIter->getRealX() < currMinA.getRealX() or
            (minAIter->getRealX() == currMinA.getRealX() and minAIter->getRealY() < currMinA.getRealY())) {
            currMinA = *minAIter;
        }
    }
    DiagramPoint optB = *(B.begin());
    for(const auto& pointB : B) {
        if (distLInf(currMinA, pointB) > result) {
            result = distLInf(currMinA, pointB);
            optB = pointB;
        }
    }
    for(const auto& pointA : A) {
        if (distLInf(pointA, optB) > result) {
            result = distLInf(pointA, optB);
        }
    }
    return result;
}


void removeDuplicates(std::vector<Point>& dgmA, std::vector<Point>& dgmB);

} // end of namespace
#endif
