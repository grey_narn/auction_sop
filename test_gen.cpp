#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <unordered_set>
#include <math.h>
#include <stdlib.h>
#include <random>
#include <sys/times.h>
#include <ctime>

using namespace std;


void getAnswer(const std::string fnameA,
               const std::string fnameB,
               const std::string fnameAnswer)
{
    // run bottleneck from Dionysus to get the correct answer
    std::string dionisysBottleneckExecutable { "/home/narn/projects/Dionysus/build/tools/matching/bottleneck" };
    std::string command { dionisysBottleneckExecutable + " " + fnameA + " " + fnameB + " | tr -dc .0123456789 > " + fnameAnswer };
    const std::string fnameExecTime = fnameAnswer + "_time";
    struct tms buf1, buf2;
    times(&buf1);
    system(command.c_str());
    times(&buf2);
    double timeElapsed { buf2.tms_cutime - buf1.tms_cutime };
    double secElapsed { timeElapsed / (double) CLOCKS_PER_SEC };
    std::ofstream fileExecTime(fnameExecTime);
    if (!fileExecTime.good()) {
        std::cerr << "Cannot write to " << fileExecTime << std::endl;
    }
    fileExecTime << "Time elapsed: " << timeElapsed << "; in seconds: " << secElapsed << std::endl;
    std::cout << fnameAnswer <<  ", time elapsed: " << timeElapsed << "; in seconds: " << secElapsed << std::endl;
    fileExecTime.close();
}


bool writeRandomDiagramUnif(const std::string diagramName, 
                            const int diagramSize,
                            const double lowerBoundHor, 
                            const double upperBoundHor,
                            const double lowerBoundVert,
                            const double upperBoundVert)
{
    if (diagramSize <= 0) {
        std::cerr << "Error! diagramSize is not positive: " << diagramSize << endl;
        return false;
    }
    static std::uniform_real_distribution<double> unifHor(lowerBoundHor, upperBoundHor);
    static std::default_random_engine randEngHor;
    static std::uniform_real_distribution<double> unifVert(lowerBoundVert, upperBoundVert);
    static std::default_random_engine randEngVert;

    std::ofstream diagramFile(diagramName);
    if (!diagramFile.good()) {
        std::cerr << "Cannot write to file " << diagramName << std::endl;
        return false;
    }

    for(int k = 0; k < diagramSize; ++k) {
        double randX = unifHor(randEngHor);
        double randY = randX + unifVert(randEngVert);
        diagramFile << randX << " " << randY << std::endl;
    }
    diagramFile.close();
    return true;
}

// create random persistence diagram with points concentrated
// near diagonal and rare persistence features
bool writeRandomDiagramReal(const std::string diagramName, 
                            const int diagramSize,
                            const double lowerBoundHor, 
                            const double upperBoundHor,
                            const double vertMean,
                            const double vertStdDev)
{
    if (diagramSize <= 0) {
        std::cerr << "Error! diagramSize is not positive: " << diagramSize << endl;
        return false;
    }

    static std::default_random_engine randEngHor;
    static std::default_random_engine randEngVert;
    
    static std::uniform_real_distribution<double> unifHor(lowerBoundHor, upperBoundHor);
    static std::uniform_real_distribution<double> vertCoin(1, 100);
    static std::normal_distribution<double> gaussVert(vertMean, vertStdDev);
    static std::exponential_distribution<double> expVert(1);

    std::ofstream diagramFile(diagramName);
    if (!diagramFile.good()) {
        std::cerr << "Cannot write to file " << diagramName << std::endl;
        return false;
    }
    diagramFile.precision(15);

    for(int k = 0; k < diagramSize; ++k) {
        double randX = unifHor(randEngHor);
        double randY = randX + abs(gaussVert(randEngVert));
        if (randY - randX < 0.01 or vertCoin(randEngVert) > 98) {  
                randY += 10000*expVert(randEngVert);
        }
        diagramFile << randX << " " << randY << std::endl;
    }
    diagramFile.close();
    return true;
}

// create random persistence diagram with points concentrated
// near diagonal and rare persistence features
bool writeRandomDiagramSopUnif(const std::string diagramName, 
                               const std::string checkFileName,
                               const int diagramSize,
                               const double lowerBoundHor, 
                               const double upperBoundHor,
                               const double lowerBoundVert, 
                               const double upperBoundVert,
                               const size_t lowerBoundMult,
                               const size_t upperBoundMult)
{
    if (diagramSize <= 0) {
        std::cerr << "Error! diagramSize is not positive: " << diagramSize << endl;
        return false;
    }

    static std::default_random_engine randEngHor;
    static std::default_random_engine randEngVert;
    static std::default_random_engine randEngM;
    
    static std::uniform_real_distribution<double> unifHor(lowerBoundHor, upperBoundHor);
    static std::uniform_real_distribution<double> unifVert(lowerBoundVert, upperBoundVert);
    static std::uniform_int_distribution<size_t> unifMult(lowerBoundMult, upperBoundMult);

    std::ofstream diagramFile(diagramName);
    std::ofstream checkFile(checkFileName);
    if (!diagramFile.good()) {
        std::cerr << "Cannot write to file " << diagramName << std::endl;
        return false;
    }

    if (!checkFile.good()) {
        std::cerr << "Cannot write to file " << checkFileName << std::endl;
        return false;
    }

    diagramFile.precision(15);
    checkFile.precision(15);

    for(int k = 0; k < diagramSize; ++k) {
        double randX = unifHor(randEngHor);
        double randY = randX + unifVert(randEngVert);
        size_t mult = unifMult(randEngM);
        diagramFile << randX << " " << randY << " " << mult << std::endl;
        for(size_t j = 0; j < mult; ++j) {
            checkFile << randX << " " << randY << std::endl;
        }
    }
    diagramFile.close();
    checkFile.close();
    return true;
}

// create random persistence diagram with points concentrated
// near diagonal and rare persistence features
bool writeDiagramSopSkew(const std::string diagramName, const std::string checkFileName,
                               const int diagramSize,
                               const double lowerBoundHor, 
                               const double upperBoundHor,
                               const double lowerBoundVert, 
                               const double upperBoundVert,
                               const size_t minNorm,
                               const size_t maxNorm,
                               const size_t numPeaks,
                               const size_t minPeak,
                               const size_t maxPeak)
{
    static std::default_random_engine randEng;
    
    static std::uniform_real_distribution<double> unifHor(lowerBoundHor, upperBoundHor);
    static std::uniform_real_distribution<double> unifVert(lowerBoundVert, upperBoundVert);
    static std::uniform_int_distribution<size_t> unifPeak(0, diagramSize - 1);
    static std::uniform_int_distribution<size_t> unifNormMult(minNorm, maxNorm);
    static std::uniform_int_distribution<size_t> unifPeakMult(minPeak, maxPeak);
    std::unordered_set<size_t> peakVertices;

    for(size_t j = 0; j < numPeaks; ++j) {
        size_t a = unifPeak(randEng);
        peakVertices.insert(a);
    }

    std::ofstream diagramFile(diagramName);
    std::ofstream checkFile(checkFileName);
    if (!diagramFile.good()) {
        std::cerr << "Cannot write to file " << diagramName << std::endl;
        return false;
    }

    if (!checkFile.good()) {
        std::cerr << "Cannot write to file " << checkFileName << std::endl;
        return false;
    }

    diagramFile.precision(15);
    checkFile.precision(15);

    for(int k = 0; k < diagramSize; ++k) {
        double randX = unifHor(randEng);
        double randY = randX + unifVert(randEng);
        size_t mult;
        if ( peakVertices.count(k) == 0) {
            mult = unifNormMult(randEng);
        } else {
            mult = unifPeakMult(randEng);
        }
        diagramFile << randX << " " << randY << " " << mult << std::endl;
        for(size_t j = 0; j < mult; ++j) {
            checkFile << randX << " " << randY << std::endl;
        }
    }
    diagramFile.close();
    checkFile.close();
    return true;
}



int main()
{
    //std::vector<int> vecSizeA; // { 1, 2, 3, 4, 5, 6, 10, 15, 20, 50, 100, 200, 300, 400, 500, 800, 1000, 1500, 2000, 3000, 5000, 10000 };
    std::string testFolder { "/home/narn/projects/auction_sop/sop_tests/test_set_8/" };

    std::vector< std::pair<size_t, size_t> > normMults { {1,1}, {1, 3}, {1, 10}  };
    std::vector< std::pair<size_t, size_t> > peakMults { {3,3}, {3, 6}, {10, 10},  {10, 20}  };
    std::vector< size_t > peakFractions { 20, 15, 10, 5, 4 };

    // uncomment for skew diagrams
    /*
    size_t testSize {0};
    size_t testNum { 0 };

    for(auto peakFraction : peakFractions) {
        for(auto normMult : normMults) {
            for(auto peakMult : peakMults) {
                for(int testSize = 4; testSize <= 30; testSize++) {
                    for(int instanceNum = 0; instanceNum < 3; ++instanceNum) {
                        testNum++;
                        std::string testPrefix { testFolder + std::string( (testNum < 10) ? "sop_test_0" : "sop_test_" ) + std::to_string(testNum) };
                        std::string checkTestPrefix { testFolder + std::string( (testNum < 10) ? "check_sop_test_0" : "check_sop_test_" ) + std::to_string(testNum) };
                        std::string fnameA { testPrefix + "_A" };
                        std::string checkFnameA { checkTestPrefix + "_A" };
                        std::string fnameB { testPrefix + "_B" };
                        std::string checkFnameB { checkTestPrefix + "_B" };
                        std::string fnameAnswer { testPrefix  + "_ans" };

                        // range for random
                        double lowerBoundHor = 1;
                        double upperBoundHor = 100;

                        double lowerBoundVert = 1;
                        double upperBoundVert = 100;

                        size_t minNorm = normMult.first;
                        size_t maxNorm = normMult.second;
                        size_t minPeak = peakMult.first;
                        size_t maxPeak = peakMult.second;
                        size_t numPeaks = static_cast<size_t> (round( static_cast<double>(testSize) / static_cast<double>(peakFraction) )) ;
                        
                        if (!writeDiagramSopSkew(fnameA, 
                                                 checkFnameA,
                                                 testSize, 
                                                 lowerBoundHor, 
                                                 upperBoundHor, 
                                                 lowerBoundVert, 
                                                 upperBoundVert, 
                                                 minNorm,
                                                 maxNorm,
                                                 numPeaks,
                                                 minPeak,
                                                 maxPeak))
                            return 1;

                        if (!writeDiagramSopSkew(fnameB, 
                                                 checkFnameB,
                                                 testSize, 
                                                 lowerBoundHor, 
                                                 upperBoundHor, 
                                                 lowerBoundVert, 
                                                 upperBoundVert, 
                                                 minNorm,
                                                 maxNorm,
                                                 numPeaks,
                                                 minPeak,
                                                 maxPeak))
                            return 1;

                        //getAnswer(fnameA, fnameB, fnameAnswer);
                    }
                }
            }
        }
    }
    return 0;
    */
   

    size_t testSize {0};
    size_t testNum { 0 };
    for(int testSize = 100; testSize <= 800; testSize += 100) {
        for(int instanceNum = 0; instanceNum < 10; ++instanceNum) {
            testNum++;
            std::string testPrefix { testFolder + std::string( (testNum < 10) ? "sop_test_0" : "sop_test_" ) + std::to_string(testNum) };
            std::string checkTestPrefix { testFolder + std::string( (testNum < 10) ? "check_sop_test_0" : "check_sop_test_" ) + std::to_string(testNum) };
            std::string fnameA { testPrefix + "_A" };
            std::string checkFnameA { checkTestPrefix + "_A" };
            std::string fnameB { testPrefix + "_B" };
            std::string checkFnameB { checkTestPrefix + "_B" };
            std::string fnameAnswer { testPrefix  + "_ans" };

            // range for random
            double lowerBoundHor = 10;
            double upperBoundHor = 1000;

            double lowerBoundVert = 10;
            double upperBoundVert = 1000;
     
            size_t lowerBoundMult = 500;
            size_t upperBoundMult = 800;
            
            if (!writeRandomDiagramSopUnif(fnameA, 
                                        checkFnameA,
                                        testSize, 
                                        lowerBoundHor, 
                                        upperBoundHor, 
                                        lowerBoundVert, 
                                        upperBoundVert, 
                                        lowerBoundMult,
                                        upperBoundMult))
                return 1;

            if (!writeRandomDiagramSopUnif(fnameB, 
                                        checkFnameB,
                                        testSize, 
                                        lowerBoundHor, 
                                        upperBoundHor, 
                                        lowerBoundVert, 
                                        upperBoundVert, 
                                        lowerBoundMult,
                                        upperBoundMult))
                return 1;

            //getAnswer(fnameA, fnameB, fnameAnswer);
        }
    }
   
    return 0;
}
