#!/bin/bash

exec_name=$1
test_file=$2
log_file=$3
ans_postfix=$4
wasser_power=$5

sop_file_name=`echo "$test_file" | sed 's/check_//'`
check_sop_file_name=`echo "$sop_file_name" | sed 's/sop_test_/check_sop_test_/'`

echo "Running ${exec_name} for ${test_file}"
# get number of points ( = number of lines)
num_pts=`wc -l "${sop_file_name}_A" | awk '{ printf "%d", $1; }'`
check_num_pts_A=`wc -l "${check_sop_file_name}_A" | awk '{ printf "%d", $1; }'`
check_num_pts_B=`wc -l "${check_sop_file_name}_B" | awk '{ printf "%d", $1; }'`
echo -n "${test_file};${num_pts};${check_num_pts_A};${check_num_pts_B};">>$log_file
#echo -n "${test_file} ${num_pts} ${check_num_pts_A} ${check_num_pts_B}"

if [ -z "$ans_postfix" ]; 
then
    echo "Answer postfix not specified"
    exit 1;
else
    redirect_ans_to="${test_file}_${ans_postfix}"
fi

#echo -n "/usr/bin/time -f%S %U %M $exec_name ${test_file}_A ${test_file}_B $wasser_power"
/usr/bin/time -f"%S %U %M" "$exec_name" "${test_file}_A" "${test_file}_B" $wasser_power \
    2>&1 1>"$redirect_ans_to" | \
    awk '{ printf "%f;%f\n", $1 + $2, $3 / 4.0 ; }' >>"$log_file"  \
    || echo $test_file >> "caused_error.lst"

