#!/bin/bash

for f_check in check_sop_test_*_ans_ws_kdtree
do
    f_sop=`echo $f_check | sed 's/check_//g' | sed 's/ws_kdtree/sop_kdtree/g'`
    #echo $f_sop
    dist_check=`cat $f_check`
    dist_sop=`cat $f_sop`
    echo "$f_check $dist_check $dist_sop" | awk   ' { if (   (( $3<0.001) && ($2 > 0.001)) || (( $3 > 0.001) &&  ( ($2-$3) / $3 > 0.01 ||  ($2-$3) / $3 < -0.01 )) ) printf "%s %f <---> %f\n", $1, $2, $3 }'
done
