CXX=g++
BUILDDIR ?= build
CFG      ?= default
NAME     ?= auction_sop_kdtree_stat_deb
SRCDIR   ?= src

Q ?= @

BINDIR := $(BUILDDIR)/$(CFG)
BIN    := $(BINDIR)/$(NAME)
SRC    := $(sort $(wildcard $(SRCDIR)/*.cpp))
OBJ    := $(SRC:$(SRCDIR)/%.cpp=$(BINDIR)/%.o)
DEP    := $(OBJ:%.o=%.d)

# -fno-omit-frame-pointer is for profiling with perf only
#CFLAGS   += -O0 -Wall -g -ggdb -fno-omit-frame-pointer 
#CFLAGS   += -O2 -Wall 
CFLAGS   += -O2 -g -Wall -DNDEBUG  -DBOOST_DISABLE_ASSERTS -fno-omit-frame-pointer
#CFLAGS   += -O2 -g -Wall -DNDEBUG  -DBOOST_DISABLE_ASSERTS -fno-omit-frame-pointer -march=native -mtune=native --fast-math
#CFLAGS   += -O2 -g -Wall -DNDEBUG  -DBOOST_DISABLE_ASSERTS  -march=native -mtune=native --fast-math
#CFLAGS   += -O3  -Wall -march=native -mtune=native --fast-math 
#CFLAGS   += -O3  -Wall -march=native -mtune=native --fast-math -DNDEBUG
INCLUDE_FLAGS = -Iinclude -Isrc
#LINK_FLAGS =  -pg 
#CFLAGS   += -O0 -Wall -g -ggdb -pg
CXXFLAGS += $(CFLAGS) -std=c++11 $(INCLUDE_FLAGS)  

DUMMY := $(shell mkdir -p $(sort $(dir $(OBJ))))

.PHONY: all clean test_gen

all: $(BIN)

# -include $(CFG).cfg

-include $(DEP)

clean:
	@echo "===> CLEAN"
	$(Q)rm -f $(BINDIR)/*.d
	$(Q)rm -f $(BINDIR)/*.o
	$(Q)rm -f $(BINDIR)/$(NAME)

$(BIN): $(OBJ)
	@echo "===> LD $@"
	$(Q)$(CXX) -o $(BIN) $(OBJ) $(LINK_FLAGS)

$(BINDIR)/%.o: $(SRCDIR)/%.cpp
	@echo "===> CXX $<"
	$(Q)$(CXX) $(CXXFLAGS) -MMD -c -o $@ $<

link:
	@echo "===> LD $@"
	$(Q)$(CXX) -o $(BIN) $(OBJ) $(LINK_FLAGS)

test:
	#tests/run_all.sh
	g++ -O2 -o test_gen test_gen.cpp -std=c++11
	./test_gen

prerequisites: all

target: test 
	g++ -O2 -o test_gen test_gen.cpp -std=c++11
	./test_gen
