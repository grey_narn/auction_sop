#include "diagonal_pool.h"

using namespace dnn;

DiagonalPool::DiagonalPool(std::vector<MassType>& diagMasses, bool _isAssigned, double _price) :
    isAssigned(_isAssigned),
    commonPrice(price),
    totalMass( std::accumulate(diagMasses.begin(), diagMasses.end(), 0)),
    masses(diagMasses)
{
    for(size_t objIdx = 0; objIdx < masses.size(); ++objIdx) {
        objectIndices.insert(objIdx, masses[objIdx]);
    }
}


double DiagonalPool::getCommonPrice() const 
{
    return commonPrice;
}

void DiagonalPool::setCommonPrice(double newValue) 
{
    commonPrice = newValue;
}

int DiagonalPool::objectIdx() 
{
    if (objectIndices.size() == 1) {
        return objectIndices.myMap.begin()->first;
    } else {
        return -1;
    }
}


void DiagonalPool::reassign(std::vector<DiagonalPool>& newPools, MassMatrix& newSliceMasses, RealMatrix newSlicePrices)
{
    //assert( newPools.size() == 0 );
    // todo: get rid of this?
    if (bidTable.size() == 0) {
        newPools.push_back(*this);
        return;
    }

    newPools.reserve(newPools.size() + bidTable.size());

    std::vector<MassType> newPoolMasses (masses.size(), 0);
    // first process bids from normal bidders, since they must get mass from
    // their projection

    for(auto bidIter = bidTable.begin(); bidIter != bidTable.end(); ++bidIter) {
        if (bidIter->bidderIdx < numNormalBidders) {
            // bidder is normal
            auto bidderIdx = bidIter->bidderIdx;
            auto bidMass = bid.mass;
            assert(masses[bidderIdx] >= bidMass);
            masses[bidderIdx] -= bidMass;
            newPoolMasses[bidderIdx] += bidMass;
            newSliceMasses[bidderIdx][bidderIdx] += bidMass;
            newSlicePrices[bidderIdx][bidderIdx] = bidIter->value;
        }
        // create new pool from accumulated masses, it is assigned and has
        // the value of bid as new price
        newPools.emplace_back(newPoolMasses, true, bidIter->value);
        // reset vector of masses for new pool
        for(auto& newMass : newPoolMasses) {
            newMass = 0;
        }
    } // end of processing bids from normal bidders
 
    size_t currentObjectIdx { 0 };
    for(auto bidIter = bidTable.ordered_begin(); bidIter != bidTable.ordered_end(); ++bidIter) {
        if ( bidIter->bidderIdx >= numNormalBidders ) {
            // accumulate masses of new pool while prices of consecutive bids are
            // the same
            auto bidMass = bid.mass;
            while(bidMass > 0) {
                assert(currentObjectIdx < masses.size());
                if ( masses[currentObjectIdx] > bidMass ) {
                    // we stay at the current object. current bid is finished
                    masses[currentObjectIdx] -= bidMass;
                    newPoolMasses[currentObjectIdx] += bidMass;
                    newSliceMasses[bidderIdx][currentObjectIdx] += bidMass;
                    newSlicePrices[bidderIdx][currentObjectIdx] = bidIter->value;
                    bidMass = 0;
                } else {
                    newSliceMasses[bidderIdx][currentObjectIdx] += masses[currentObjectIdx];
                    newSlicePrices[bidderIdx][currentObjectIdx] = bidIter->value;
                    bidMass -= masses[currentObjectIdx];
                    newPoolMasses[currentObjectIdx] += masses[currentObjectIdx];
                    masses[currentObjectIdx] = 0;
                    currentObjectIdx++;
                }
            } // end of bid processing

            if ( std::next(bidIter) != bidTable.end() or std::next(bidIter)->value == bidIter->value) {
                // create new pool from accumulated masses, it is assigned and has
                // the value of bid as new price
                newPools.emplace_back(newPoolMasses, true, bidIter->value);
                // reset vector of masses for new pool
                for(auto& newMass : newPoolMasses) {
                    newMass = 0;
                }
            } // otherwise the next bid has the same price and we do not reset newPoolMasses
        }
    }
    // if something remains in the pool , add it, too
    if ( totalMass > bidTable.totalMass ) {
        newPools.push_back( *this );
    }
}


void DiagonalPool::mergeWithPool(DiagonalPool& anotherPool)
{
    assert(isAssigned);
    assert(anotherPool.isAssigned);
    assert(masses.size() == anotherPool.masses.size());
    for(size_t objIdx = 0; objIdx < masses.size(); ++objIdx) {
        masses[objIdx] += anotherPool.masses[objIdx];
    }
}


MassType DiagonalPool::getAvailableMass(size_t bidderIdx)
{
    MassType result;
    if (isAssigned) {
        result = totalMass - masses[bidderIdx];
    } else {
        result = totalMass;
    }
    return result;
}


DiagonalPoolHeap::DiagonalPoolHeap(AuctionKdtreeRunner* _auctionRunner) :
    auctionRunner(_auctionRunner)
{
}

DiagonalPoolHeap::PoolIterator DiagonalPoolHeap::ordered_begin()
{
    // quick and dirty: just sort pools
    // TODO: replace with a decent heap?
    //std::sort(pools.begin(), pools.end(), CompDiagonalPools());
    return pools.begin();
}

DiagonalPoolHeap::PoolIterator DiagonalPoolHeap::ordered_end()
{
    // quick and dirty: just sort pools
    // TODO: replace with a decent heap?
    return pools.end();
}

void DiagonalPoolHeap::reassign()
{
    RealMatrix newSlicePrices;
    MassMatrix newSliceMasses;
    // TODO: merge different pools with the same price 
    //std::map<double, std::vector<DiagonalPool>::iterator> pricesToPools;
    std::vector<DiagonalPool> newPools;
    size_t approxSize { 0 };
    for(auto& pool : pools) {
        approxSize += (pools.bidTable.size() + 1);
    }
    // TODO: loop only over pools with bids
    newPools.reserve(approxSize);
    for(auto& pool : pools) {
        pool.reassign(newPools);
    }
    std::sort(newPools.begin(), newPools.end(), CompDiagonalPools());
    pools = newPools;
    for(size_t poolIdx = 0; poolIdx < pools.size(); ++poolIdx) {
        pools[poolIdx].id = poolIdx;
    }
}
