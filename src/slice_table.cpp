#include "slice_table.h"
#include "auction_kdtree_gs.h"


namespace auction_sop {

std::ostream& operator<<(std::ostream& output, const SliceIdx& s){
    output << "SliceIdx( owner: ";
    if ( s.ownerIdx != AuctionKdtreeRunnerGS::NO_OWNER_IDX)
        output << s.ownerIdx << ", objIdx: " << s.objectIdx << ", assigned: " << std::boolalpha << s.isAssigned << ")";
    else 
        output << "NONE, objIdx: " << s.objectIdx << ", assigned: " << std::boolalpha << s.isAssigned << ")";
    return output;
}

std::ostream& operator<<(std::ostream& output, const PointSlice& ps){
    output << "PointSlice: [ " << ps.sliceIdx << ", mass = " << ps.mass << ", price = " << ps.price << ", persistence = " << ps.persistence << ", diag.loss = " << ps.getLossForDiagonal() << "] ";
    return output;
}

std::ostream& operator<<(std::ostream& output, const PointSliceHeapAsc& ps){
    output << "-------------------------------------------------------------------" << std::endl;
    for(const auto& s : ps) {
        output << s << std::endl;
    }
    output << "----------------------------------------------------------------end" << std::endl;
    return output;
}

std::ostream& operator<<(std::ostream& output, const PointSliceHeapDiag& ps){
    output << "-------------------------------------------------------------------" << std::endl;
    for(const auto& s : ps) {
        output << s << std::endl;
    }
    output << "----------------------------------------------------------------end" << std::endl;
    return output;
}
}
