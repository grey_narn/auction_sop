#include "stat_keeper.h"

namespace auction_sop {
//  ------------- StatKeeper -------------

StatKeeper::StatKeeper(const std::string statFileName)
{
#ifdef GATHER_STAT_KDTREE 
    if (not statFileName.empty() ) {
        output.open(statFileName.c_str());
        if (not output.good() ) {
            std::cerr << "cannot write stats to file " << statFileName << std::endl;
        } else {
            output << "iterNum;phaseNum;unassignedMass;unassignedBidders;totalSlices;totalNumDiagSlices;totalNumNormalSlices;numSlicesErased;numSlicesCreated;priceHeapTraversal;pushesToPriceHeap;pushesToObjectBidHeap;pushesToSliceHeap;decreasePriceKey" << std::endl;
            output.flush();
        }
    }
#endif
}

void StatKeeper::printStats()
{
#ifdef GATHER_STAT_KDTREE 
    if (output.good()) {
        output << iterNum << ";" << phaseNum << ";" << unassignedMass << ";" << unassignedBidders << ";" << totalNumSlices << ";" << totalNumDiagSlices << ";" << totalNumNormalSlices << ";" << numSlicesErased << ";" << numSlicesCreated << ";" << priceHeapTraversal << ";" << pushesToPriceHeap << ";" << pushesToObjectBidHeap << ";" << pushesToSliceHeap << ";" << decreasePriceKey << std::endl;
        output.flush();
    } else  {
        std::cout << iterNum << ";" << phaseNum << ";" << unassignedMass << ";" << unassignedBidders << ";" << totalNumSlices << ";" << totalNumDiagSlices << ";" << totalNumNormalSlices << ";" << numSlicesErased << ";" << numSlicesCreated << ";" << priceHeapTraversal << ";" << pushesToPriceHeap << ";" << pushesToObjectBidHeap << ";" << pushesToSliceHeap << ";" << decreasePriceKey << std::endl;
    }
#endif
}

StatKeeper::~StatKeeper()
{
#ifdef GATHER_STAT_KDTREE 
    if (output.good()) {
        output.close();
    }
#endif
}

void StatKeeper::resetForIteration()
{
#ifdef GATHER_STAT_KDTREE 
    resetForPhase();
    iterNum++;
    phaseNum = 0;
    unassignedMass = 0;
    unassignedBidders = 0;
    numSlicesErased = 0;
    numSlicesCreated = 0;
    priceHeapTraversal = 0;
#endif
}

void StatKeeper::resetForPhase()
{
#ifdef GATHER_STAT_KDTREE 
    phaseNum++;
    numSlicesErased = 0;
    numSlicesCreated = 0;
    priceHeapTraversal = 0;
    pushesToPriceHeap= 0;
    pushesToObjectBidHeap= 0;
    pushesToSliceHeap = 0;
    decreasePriceKey = 0;
#endif
}


//  ------------- end of StatKeeper -------------



} // end of namespace auction_sop
