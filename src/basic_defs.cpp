#include <algorithm>
#include <cfloat>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <math.h>


#include "basic_defs.h"
#include "my_multiset.h"

namespace auction_sop {
// Point

bool Point::operator==(const Point& other) const
{
    return ((this->x == other.x) and (this->y == other.y));
}

bool Point::operator!=(const Point& other) const
{
    return !(*this == other);
}

std::ostream& operator<<(std::ostream& output, const Point p)
{ output << "(" << p.x << ", " << p.y << ")";
    return output;
}

// WeightedPoint

bool WeightedPoint::operator==(const WeightedPoint& other) const
{
    return (this->x == other.x) and (this->y == other.y) and (this->w == other.w);
}

bool WeightedPoint::operator!=(const WeightedPoint& other) const
{
    return !(*this == other);
}

std::ostream& operator<<(std::ostream& output, const WeightedPoint p)
{
    output << "(" << p.x << ", " << p.y << " " << p.w << ")";
    return output;
}

std::ostream& operator<<(std::ostream& output, const PointSet& ps)
{
    output << "{ ";
    for(auto& p : ps) {
        output << p << ", ";
    }
    output << "\b\b }";
    return output;
}

double sqrDist(const Point& a, const Point& b)
{
    return (a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y);
}

double dist(const Point& a, const Point& b)
{
    return sqrt(sqrDist(a, b));
}

double distLInf(const Point& a, const Point& b)
{
    return std::max(fabs(a.x - b.x), fabs(a.y - b.y));
}

// DiagramPoint

// compute l-inf distance between two diagram points
double distLInf(const DiagramPoint& a, const DiagramPoint& b)
{
    if ( a.isDiagonal() and b.isDiagonal() ) {
        // distance between points on the diagonal is 0
        return 0.0;
    } else {
        return std::max(fabs(a.getRealX() - b.getRealX()), fabs(a.getRealY() - b.getRealY()));
    }
}

bool DiagramPoint::operator==(const DiagramPoint& other) const
{
    return ((this->x == other.x) and (this->y == other.y) and (this->type == other.type));
}

bool DiagramPoint::operator!=(const DiagramPoint& other) const
{
    return !(*this == other);
}

std::ostream& operator<<(std::ostream& output, const DiagramPoint p)
{
    output << "[";
    output << "(" << p.getRealX() << ", " << p.getRealY() << ") ";
    if (p.isDiagonal()) {
        output << " DIAG, ";
    } else {
        output << " NORM, ";
    }
    output << p.getMass() << "]";
    return output;
}

std::ostream& operator<<(std::ostream& output, const DiagramPointSet& ps)
{
    output << "{ ";
    for(auto pit = ps.cbegin(); pit != ps.cend(); ++pit) {
        output << *pit << ", ";
    }
    output << "\b\b }";
    return output;
}

DiagramPoint::DiagramPoint(double xx, double yy, MassType w, Type ttype) :
    x(xx),
    y(yy),
    mass(w),
    type(ttype)
{
    if (w < 0)
        throw "Negative weigt";

}

DiagramPoint::DiagramPoint(const Point& pt, const MassType _w) :
    x(pt.x),
    y(pt.y),
    mass(_w),
    type(DiagramPoint::NORMAL)
{
}

void DiagramPointSet::insert(const DiagramPoint p)
{
    points.insert(p);
}

Point DiagramPoint::getOrdinaryPoint() const
{
    return Point(getRealX(), getRealY());
}

double DiagramPoint::getRealX() const
{
    if (isNormal())
        return x;
    else
        return 0.5 * ( x + y);
}

double DiagramPoint::getRealY() const
{
    if (isNormal())
        return y;
    else
        return 0.5 * ( x + y);
}

double DiagramPoint::getPersistence() const
{
    if (isNormal())
        return 0.5 * ( y - x);
    else
        return 0.0;
}
// erase should be called only for the element of the set
void DiagramPointSet::erase(const DiagramPoint& p, bool doCheck)
{
    auto it = points.find(p);
    if (it != points.end()) {
        points.erase(it);
    } else {
        assert(!doCheck);
    }
}

void DiagramPointSet::reserve(const size_t newSize)
{
    points.reserve(newSize);
}


void DiagramPointSet::erase(const std::unordered_set<DiagramPoint, DiagramPointHash>::const_iterator it)
{
    points.erase(it);
}

void DiagramPointSet::clear()
{
    points.clear();
}

size_t DiagramPointSet::size() const
{
    return points.size();
}

bool DiagramPointSet::empty() const
{
    return points.empty();
}

bool DiagramPointSet::hasElement(const DiagramPoint& p) const
{
    return points.find(p) != points.end();
}


bool readDiagramPointSet(const std::string& fname, std::vector<Point>& result)
{
    return readDiagramPointSet(fname.c_str(), result);
}


void transformToWeightedPoints(const std::vector<Point>& input, std::vector<DiagramPoint>& output)
{
    std::unordered_map<Point, size_t, PointHash> dgm;
    for(const auto& p : input) {
        dgm[p]++;
    }
    output.clear();
    output.reserve(dgm.size());
    for(const auto& point_mass_pair : dgm) {
        output.push_back(DiagramPoint(point_mass_pair.first.x, point_mass_pair.first.y, point_mass_pair.second, DiagramPoint::NORMAL));
    }
}


bool readDiagramPointSet(const char* fname, std::vector<Point>& result)
{
    size_t lineNumber { 0 };
    result.clear();
    std::ifstream f(fname);
    if (!f.good()) {
        std::cerr << "Cannot open file " << fname << std::endl;
        return false;
    }
    std::string line;
    while(std::getline(f, line)) {
        lineNumber++;
        // process comments: remove everything after hash
        auto hashPos = line.find_first_of("#", 0);
        if( std::string::npos != hashPos) {
            line = std::string(line.begin(), line.begin() + hashPos);
        }
        if (line.empty()) {
            continue;
        }
         // trim whitespaces
        auto whiteSpaceFront = std::find_if_not(line.begin(),line.end(),isspace);
        auto whiteSpaceBack = std::find_if_not(line.rbegin(),line.rend(),isspace).base();
        if (whiteSpaceBack <= whiteSpaceFront) {
            // line consists of spaces only - move to the next line
            continue;
        }
        line = std::string(whiteSpaceFront,whiteSpaceBack);
        double x, y;
        std::istringstream iss(line);
        if (not(iss >> x >> y)) {
            std::cerr << "Error in file " << fname << ", line number " << lineNumber << ": cannot parse \"" << line << "\"" << std::endl;
            return false;
        }
        if ( x != y) {
            result.push_back(Point(x,y));
        }
    }
    f.close();
    return true;
}

void snap_diagram_to_grid(const std::vector<Point>& dgm_in,
        const double grid_size,
        std::vector<DiagramPoint>& dgm_out_normal,
        std::vector<DiagramPoint>& dgm_out_diagonal,
        double& error_estimate)
{
    assert(grid_size > 0);
    dgm_out_normal.clear();
    dgm_out_diagonal.clear();
    error_estimate = 0.0;
    size_t numDiscardedPts = 0;
    std::unordered_map<Point, int, PointHash> weightedDgmMultiset;

    for(const auto& p : dgm_in) {
        double grid_x { floor(0.5 + p.x / grid_size) };
        double grid_y { ceil(-0.5 + p.y / grid_size) };
        if ( grid_x != grid_y)
            weightedDgmMultiset[Point(grid_x, grid_y)]++;
        else {
            error_estimate += (p.y - p.x);
            numDiscardedPts++;
        }
    }
    dgm_out_normal.reserve(weightedDgmMultiset.size());
    dgm_out_diagonal.reserve(weightedDgmMultiset.size());
    for(auto pointWeightPair : weightedDgmMultiset) {
        dgm_out_normal.emplace_back(pointWeightPair.first.x, pointWeightPair.first.y, pointWeightPair.second, DiagramPoint::NORMAL);
        dgm_out_diagonal.emplace_back(pointWeightPair.first.x, pointWeightPair.first.y, pointWeightPair.second, DiagramPoint::DIAG);
    }
    std::cout << "snap_diagram_to_grid: discarded " << numDiscardedPts << " out of " << dgm_in.size() << ", error estimate " << error_estimate << std::endl;
}

void snapDiagramToDiamondGrid(const std::vector<Point>& dgmIn,
                              const double gridSize,
                              std::vector<DiagramPoint>& dgmOutNormal,
                              std::vector<DiagramPoint>& dgmOutDiagonal)
{
    assert(gridSize > 0);
    dgmOutNormal.clear();
    dgmOutDiagonal.clear();
    size_t numDiscardedPts = 0;
    std::unordered_map<Point, int, PointHash> weightedDgmMultiset;

    for(const auto& p : dgmIn) {
        // rotate coordinate system by 45 degrees
        // missing sqrt(2) factor is compensated in back transform
        double rotatedX = (p.x + p.y);
        double rotatedY = (p.y - p.x);
        double gridRotatedX { floor(0.5 + rotatedX / (sqrt(2) * gridSize) ) };
        double gridRotatedY { floor(0.5 + rotatedY / (sqrt(2) * gridSize) ) };
        if ( (int)gridRotatedY != 0 )
            weightedDgmMultiset[Point(gridRotatedX, gridRotatedY)]++;
        else {
            //std::cout << "discarding, original coords " << p.x << ", " <<  p.y << ", transformed y = " << rotatedY << std::endl;
            numDiscardedPts++;
        }
    }
    dgmOutNormal.reserve(weightedDgmMultiset.size());
    dgmOutDiagonal.reserve(weightedDgmMultiset.size());
    for(auto pointWeightPair : weightedDgmMultiset) {
        // rotate back to original system
        double originalX = 0.5 * ( pointWeightPair.first.x - pointWeightPair.first.y);
        double originalY = 0.5 * ( pointWeightPair.first.x + pointWeightPair.first.y);

        dgmOutNormal.emplace_back(originalX, originalY, pointWeightPair.second, DiagramPoint::NORMAL);
        dgmOutDiagonal.emplace_back(originalX, originalY, pointWeightPair.second, DiagramPoint::DIAG);
    }
    std::cout << "snapDiagramToDiamondGrid: discarded " << numDiscardedPts << " out of " << dgmIn.size() << std::endl;
}


void snapDiagramToDiamondGrid(const std::vector<Point>& dgmIn,
                              const double gridSize,
                              std::vector<DiagramPoint>& dgmOutNormal,
                              std::vector<DiagramPoint>& dgmOutDiagonal,
                              std::unordered_map<Point, std::vector<Point>, PointHash>& backInfo,
                              std::unordered_set<Point, PointHash>& discardedPoints)
{
    assert(gridSize > 0);
    dgmOutNormal.clear();
    dgmOutDiagonal.clear();
    size_t numDiscardedPts = 0;
    std::unordered_map<Point, int, PointHash> weightedDgmMultiset;
    std::unordered_map<Point, std::vector<Point>, PointHash> backInfoTransit;
    backInfo.clear();
    discardedPoints.clear();

    for(const auto& p : dgmIn) {
        // rotate coordinate system by 45 degrees
        // missing sqrt(2) factor is compensated in back transform
        double rotatedX = (p.x + p.y) / sqrt(2.0);
        double rotatedY = (p.y - p.x) / sqrt(2.0);
        double gridRotatedX = gridSize * floor(0.5 + rotatedX / gridSize );
        double gridRotatedY = gridSize * floor(0.5 + rotatedY / gridSize);
        if ( gridRotatedY >= gridSize ) {
            weightedDgmMultiset[Point(gridRotatedX, gridRotatedY)]++;
            backInfoTransit[Point(gridRotatedX, gridRotatedY)].push_back(p);
        } else {
            //std::cout << "discarding, original coords " << p.x << ", " <<  p.y << ", transformed y = " << rotatedY << std::endl;
            discardedPoints.insert(p);
            numDiscardedPts++;
        }
    }
    dgmOutNormal.reserve(weightedDgmMultiset.size());
    dgmOutDiagonal.reserve(weightedDgmMultiset.size());
    for(auto pointWeightPair : weightedDgmMultiset) {
        // rotate back to original system
        double originalX = ( pointWeightPair.first.x - pointWeightPair.first.y) / sqrt(2.0);
        double originalY = ( pointWeightPair.first.x + pointWeightPair.first.y) / sqrt(2.0);

        backInfo[Point(originalX, originalY)] = backInfoTransit[pointWeightPair.first];

        dgmOutNormal.emplace_back(originalX, originalY, pointWeightPair.second, DiagramPoint::NORMAL);
        dgmOutDiagonal.emplace_back(originalX, originalY, pointWeightPair.second, DiagramPoint::DIAG);
    }
    std::cout << "snapDiagramToDiamondGrid: discarded " << numDiscardedPts << " out of " << dgmIn.size() << std::endl;
}

void transform_to_diagrams(const std::vector<Point>& input_points_A,
                           const std::vector<Point>& input_points_B,
                           std::vector<DiagramPoint>& dgm_A,
                           std::vector<DiagramPoint>& dgm_B,
                           bool remove_duplicates)
{
    std::unordered_map<Point, int, PointHash> map_A, map_B;
    // copy points to maps
    for(const auto& p_A : input_points_A) {
        map_A[p_A]++;
    }
    for(const auto& p_B : input_points_B) {
        map_B[p_B]++;
    }

    size_t size_A = map_A.size();
    size_t size_B = map_B.size();

    if (remove_duplicates) {
        for(auto& point_multiplicity_pair : map_A) {
            auto iter_B = map_B.find(point_multiplicity_pair.first);
            if (iter_B != map_B.end()) {
                int duplicate_multiplicity = std::min(point_multiplicity_pair.second, iter_B->second);
                point_multiplicity_pair.second -= duplicate_multiplicity;
                iter_B->second -= duplicate_multiplicity;
                if (!point_multiplicity_pair.second) {
                    --size_A;
                }
                if (!iter_B->second) {
                    --size_B;
                }
           }
        }
    }

    // clear result vectors
    dgm_A.clear();
    dgm_B.clear();

    dgm_A.reserve(size_A);
    dgm_B.reserve(size_B);

    // copy points with masses to vectors

    for(const auto& point_multiplicity_pair_A : map_A) {
        assert( point_multiplicity_pair_A.second >= 0);
        if (point_multiplicity_pair_A.second > 0) {
            dgm_A.emplace_back(point_multiplicity_pair_A.first,
                    point_multiplicity_pair_A.second);
        }
    }

    for(const auto& point_multiplicity_pair_B : map_B) {
        assert( point_multiplicity_pair_B.second >= 0);
        if (point_multiplicity_pair_B.second > 0) {
            dgm_B.emplace_back(point_multiplicity_pair_B.first,
                    point_multiplicity_pair_B.second);
        }
    }

}

} // end of namespace auction_sop
