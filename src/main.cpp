#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <algorithm>
#include <limits>
#include <random>
#include <chrono>
#include <thread>
#include <random>

#include "basic_defs.h"
#include "auction_kdtree_gs.h"

#define USE_DIAMOND_GRID

typedef std::chrono::high_resolution_clock  hr_clock;

using namespace auction_sop;


double wassersteinDistApprox(std::vector<auction_sop::DiagramPoint>& setAPoints,
        std::vector<auction_sop::DiagramPoint>& setADiagPoints,
        std::vector<auction_sop::DiagramPoint>& setBPoints,
        std::vector<auction_sop::DiagramPoint>& setBDiagPoints,
        double wassersteinPower,
        double delta,
        const char* fname)
{
    std::string fnameA = std::string(fname);
    std::string fnameStat = fnameA;
    std::replace( fnameStat.begin(), fnameStat.end(), '/', '_');
    fnameStat += "_stat.txt";
    //AuctionTranspRunner auctionRunner(A, B, wassersteinPower);
    auction_sop::AuctionKdtreeRunnerGS auctionRunner(setAPoints, setBPoints, wassersteinPower, delta, fnameStat);
    //AuctionSimpleRunner auctionRunner(A, B, wassersteinPower);
    return auctionRunner.getWassersteinDistance();
}

bool writeDiagram(const std::vector<auction_sop::DiagramPoint>& diag, std::string diagramName)
{
    std::ofstream diagramFile(diagramName);
    if (!diagramFile.good()) {
        std::cerr << "Cannot write to file " << diagramName << std::endl;
        return false;
    }

    //diagramFile.precision(15);

    for(const auto& p : diag) {
        if (p.isNormal())
            diagramFile << p.getRealX() << " " << p.getRealY() << std::endl;
    }
    diagramFile.close();
    return true;
}

bool writeWeightedDiagramAsNormal(std::vector<auction_sop::DiagramPoint>& diag, std::string diagramName)
{
    std::ofstream diagramFile(diagramName);
    if (!diagramFile.good()) {
        std::cerr << "Cannot write to file " << diagramName << std::endl;
        return false;
    }

    diagramFile.precision(18);

    for(const auto& p : diag) {
        if (p.isNormal())
            for(int copy_idx = 0; copy_idx < p.getMass(); ++copy_idx)
                diagramFile << p.getRealX() << " " << p.getRealY() << std::endl;
    }

    diagramFile.close();
    return true;
}

void perturbDiagram(const std::vector<auction_sop::DiagramPoint>& diagramIn, const double epsilon, std::vector<auction_sop::DiagramPoint>& diagramOut)
{
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<double> dist(-epsilon, epsilon);
    diagramOut.clear();
    diagramOut.reserve(diagramIn.size());
    for(const auto& p : diagramIn) {
        if (p.isNormal()) {
            for(int k = 0; k < p.getMass(); ++k) {
                double newX = p.getRealX() + dist(mt);
                double newY = p.getRealY() + dist(mt);
                double newDiagX = 0.5 * (newX + newY);
                if (newY < newX)
                    std::swap(newX, newY);
                diagramOut.push_back(auction_sop::DiagramPoint(newX, newY, 1, auction_sop::DiagramPoint::NORMAL));
                diagramOut.push_back(auction_sop::DiagramPoint(newDiagX, newDiagX, 1, auction_sop::DiagramPoint::DIAG));
            }
        }
    }
}


void multiplyPoints(const std::vector<auction_sop::Point>& dgmIn, std::vector<auction_sop::DiagramPoint>& dgmOutNorm, std::vector<auction_sop::DiagramPoint>& dgmOutDiag, const int multiplicity)
{
    dgmOutNorm.clear();
    dgmOutDiag.clear();
    for(auto p : dgmIn) {
        dgmOutNorm.emplace_back(p.x, p.y, multiplicity, DiagramPoint::NORMAL);
        dgmOutDiag.emplace_back(p.x, p.y, multiplicity, DiagramPoint::DIAG);

    }
}

void multiplyPointsRandom(const std::vector<auction_sop::Point>& dgmIn, std::vector<auction_sop::DiagramPoint>& dgmOutNorm, std::vector<auction_sop::DiagramPoint>& dgmOutDiag, const int multiplicity)
{
    dgmOutNorm.clear();
    dgmOutDiag.clear();
    std::default_random_engine randGen;
    std::uniform_int_distribution<int> distr(multiplicity / 2, (3 * multiplicity ) / 2);
    for(auto p : dgmIn) {
        if (multiplicity == 1) {
            dgmOutNorm.emplace_back(p.x, p.y, multiplicity, DiagramPoint::NORMAL);
            dgmOutDiag.emplace_back(p.x, p.y, multiplicity, DiagramPoint::DIAG);
        } else {
            int currMult = distr(randGen);
            dgmOutNorm.emplace_back(p.x, p.y, currMult, DiagramPoint::NORMAL);
            dgmOutDiag.emplace_back(p.x, p.y, currMult, DiagramPoint::DIAG);
        }
    }
}

void multiplyPointsRandom1(const std::vector<auction_sop::Point>& dgmIn, std::vector<auction_sop::DiagramPoint>& dgmOutNorm, std::vector<auction_sop::DiagramPoint>& dgmOutDiag, const int low, const int high, const double coinProb)
{
    dgmOutNorm.clear();
    dgmOutDiag.clear();
    std::default_random_engine randGen;
    std::uniform_int_distribution<int> distr(low, high);
    std::uniform_real_distribution<double> coin(0,1);
    for(auto p : dgmIn) {
        double coinRes = coin(randGen);
        int currMult = 1;
        if (coinRes < coinProb)  {
            currMult = distr(randGen);
        }
        dgmOutNorm.emplace_back(p.x, p.y, currMult, DiagramPoint::NORMAL);
        dgmOutDiag.emplace_back(p.x, p.y, currMult, DiagramPoint::DIAG);
    }
}

int main(int argc, char* argv[])
{

    if (argc < 3 ) {
        std::cerr << "Usage: " << argv[0] << " file1 file2 [wasserstein_degree] [relative_error] [internal norm]. By default power is 1.0, relative error is 0.01, internal norm is l_infinity." << std::endl;
        return 1;
    }

    std::vector<auction_sop::Point> points_1, points_2;
    std::vector<auction_sop::DiagramPoint> dgm_1, dgm_2;
    std::string fname_1(argv[1]);
    std::string fname_2(argv[2]);
    if (!readDiagramPointSet(fname_1,  points_1)) {
        std::cout << "Cannot read file " << argv[1] << "_A" << std::endl;
        std::exit(1);
    }

    if (!readDiagramPointSet(fname_2, points_2)) {
        std::cout << "Cannot read file " << argv[1] << "_B" << std::endl;
        std::exit(1);
    }

    double wasser_degree = (4 <= argc) ? atof(argv[3]) : 1.0;
    if (wasser_degree < 1.0) {
        std::cerr << "The third argument (wasserstein_degree) was \"" << argv[3] << "\", must be a number >= 1.0. Cannot proceed. " << std::endl;
        std::exit(1);
    }

    transform_to_diagrams(points_1, points_2, dgm_1, dgm_2, wasser_degree == 1.0);

    double delta = (5 <= argc) ? atof(argv[4]) : 0.01;

    double initial_epsilon = 0.0;
    double eps_factor = 6.0;

    std::string fname_stat = ""; // empty file name - no statistics written

    AuctionKdtreeRunnerGS auction_runner(dgm_1,
                                         dgm_2,
                                         wasser_degree,
                                         delta,
                                         fname_stat,
                                         initial_epsilon,
                                         eps_factor);

    std::cout << auction_runner.getWassersteinDistance() << std::endl;

    return 0;

}
