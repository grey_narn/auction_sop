#include <limits>

#include "def_debug.h"
#include "bid_table.h"

namespace auction_sop 
{


bool compareBidsByloss(const Bid& b1, const Bid& b2) { return b1.loss  < b2.loss; };

bool StupidBidTable::isFull(void) const
{
    assert(totalMass <= capacity);
    return totalMass == capacity;
}

std::vector<double> StupidBidTable::getDebuglosss() 
{
    std::vector<double> result;
    for(auto bid : bidList) {
        for(int k = 0; k < bid.mass; ++k) {
            result.push_back(bid.loss);
        }
    }
    return result;
}

void StupidBidTable::sanityCheck(void) const
{
#ifdef DEBUG_AUCTION_KDTREE
    assert(capacity > 0);
    assert(totalMass >= 0 and totalMass <= capacity);
    
    for(auto listIter = bidList.begin(); listIter != bidList.end(); ++listIter) {
        if (std::next(listIter) != bidList.end())
            assert( listIter->loss <= std::next(listIter)->loss);
    }
    MassType realTotalMass { 0 };
    for(auto bid : bidList) {
        assert( bid.mass > 0);
        realTotalMass += bid.mass;
    }
    assert(realTotalMass == totalMass);
#endif
}

void StupidBidTable::push_back(const Bid& cand)
{
    //std::cout << "inserting cand = " << cand << std::endl;
    //std::cout << "bid tbale " << *this << std::endl;
    sanityCheck();
    assert(cand.mass > 0);
    assert(totalMass < capacity);
    if (totalMass >= capacity) {
        throw "Wrong insertion in bid table";
    }
    objectSet.insert(cand.slice.getObjectIdx());
    bidList.push_back(cand);
    totalMass += cand.mass;
    if (totalMass > capacity) {
        bidList.back().mass -= ( totalMass - capacity);
        totalMass = capacity;
        cutLastEntry = true;
    }
    sanityCheck();
    //std::cout << "after inserting " << cand << std::endl << *this << std::endl;
}

 // ---- end of BidTable
void BidHeap::push(const Bid& bid)
{ 
    container.push_back(bid); 
    push_heap(container.begin(), container.end(), comp); 
}

void BidHeap::push(size_t bidderIdx, const PointSlice& slice, MassType mass, double loss, MassType availableMass) 
{ 
    container.emplace_back(bidderIdx, slice, mass, loss, availableMass);
    push_heap(container.begin(), container.end(), comp);
} 


void BidHeap::pop() 
{
    pop_heap(container.begin(), container.end(), comp);
    container.pop_back();
}
 
void BidTable::insert(const size_t bidderIdx, const PointSlice& slice, const MassType bidMass, const double loss, const MassType availableMass)
{
    // if bid has no chance, reject it
    if (loss > getW()) 
        return;
    // process slice for wHeap:
    // 1. insert new bid into heap
    // to-do: add slice idx or pointer to slice?
    wHeap.push(bidderIdx, slice, bidMass, loss, availableMass);
    objectSet.insert(slice.getObjectIdx(), availableMass);
    wHeapAccumulatedMass += availableMass;
    // 2. remove excess bids. bids are not cut in wHeap
    while(true) {
        Bid worstWBid = wHeap.top();
        if (wHeapAccumulatedMass - worstWBid.availableMass < capacity + 1) {
            break;
        }
        if (objectSet.size() == 1) {
            break;
        }
        if (objectSet.size() == 2 and
            static_cast<MassType>(objectSet.multiplicity(worstWBid.slice.getObjectIdx())) == worstWBid.availableMass) {
            break;
        }
        wHeapAccumulatedMass -= worstWBid.availableMass;
        wHeap.pop();
        objectSet.erase(worstWBid.slice.getObjectIdx(), worstWBid.availableMass);
    }
    // process slice for bidHeap
    // if current slice is inferior to what we have in bidHeap,
    // this point can be ignored.
    if ( bidHeapAccumulatedMass >= capacity and loss >= bidHeap.top().loss) {
        return; // is this correct? 
    }
    // 1. insert new bid into bidHeap
    bidHeap.push(bidderIdx, slice, bidMass, loss, availableMass);
    bidHeapAccumulatedMass += bidMass;
    // 2. remove excess bids
    while(true) {
        Bid worstBid = bidHeap.top();
        if (bidHeapAccumulatedMass - worstBid.mass < capacity) {
            break;
        }
        bidHeapAccumulatedMass -= worstBid.mass;
        bidHeap.pop();
    }
    if ( bidHeapAccumulatedMass > capacity ) {
        bidHeap.top().mass -= ( bidHeapAccumulatedMass - capacity);
        bidHeapAccumulatedMass = capacity;
        cutLastEntry = true;
    } else if (bidHeapAccumulatedMass == capacity ) {
        cutLastEntry = false;
    }
    sanityCheck();
}

void BidTable::insert(const Bid& newBid)
{
    // if bid has no chance, reject it
    if (newBid.loss > getW()) 
        return;
    // process slice for wHeap:
    // 1. insert new bid into heap
    // to-do: add slice idx or pointer to slice?
    wHeap.push(newBid);
    objectSet.insert(newBid.slice.getObjectIdx(), newBid.availableMass);
    wHeapAccumulatedMass += newBid.availableMass;
    // 2. remove excess bids. bids are not cut in wHeap
    while(true) {
        Bid worstWBid = wHeap.top();
        if (wHeapAccumulatedMass - worstWBid.availableMass < capacity + 1) {
            break;
        }
        if (objectSet.size() == 1) {
            break;
        }
        if (objectSet.size() == 2 and
            static_cast<MassType>(objectSet.multiplicity(worstWBid.slice.getObjectIdx())) == worstWBid.availableMass) {
            break;
        }
        wHeapAccumulatedMass -= worstWBid.availableMass;
        wHeap.pop();
        objectSet.erase(worstWBid.slice.getObjectIdx(), worstWBid.availableMass);
    }
    // process slice for bidHeap
    // if current slice is inferior to what we have in bidHeap,
    // this point can be ignored.
    if ( bidHeapAccumulatedMass >= capacity and newBid.loss >= bidHeap.top().loss) {
        return; // is this correct? 
    }
    // 1. insert new bid into bidHeap
    bidHeap.push(newBid);
    bidHeapAccumulatedMass += newBid.mass;
    // 2. remove excess bids
    while(true) {
        Bid worstBid = bidHeap.top();
        if (bidHeapAccumulatedMass - worstBid.mass < capacity) {
            break;
        }
        bidHeapAccumulatedMass -= worstBid.mass;
        bidHeap.pop();
    }
    if ( bidHeapAccumulatedMass > capacity ) {
        bidHeap.top().mass -= ( bidHeapAccumulatedMass - capacity);
        bidHeapAccumulatedMass = capacity;
        cutLastEntry = true;
    } else if (bidHeapAccumulatedMass == capacity ) {
        cutLastEntry = false;
    }
    sanityCheck();
}

double BidTable::getW() const
{
    if (not isFull())
        return std::numeric_limits<double>::max();
    else
        return (const_cast<BidHeap&>(wHeap)).top().loss;
}

bool BidTable::isFull() const
{
    return wHeapAccumulatedMass >= capacity + 1 and objectSet.size() > 1;
}

void BidTable::sanityCheck() const
{
#ifdef DEBUG_AUCTION_KDTREE
    assert(capacity > 0);
    assert(bidHeapAccumulatedMass <= capacity and bidHeapAccumulatedMass >= 0);
    getDebuglosss();
#endif
}

std::vector<double> BidTable::getDebuglosss()
{
    std::vector<double> resShort, resW;
    auto bidHeapIter = bidHeap.ordered_begin();
    while(bidHeapIter != bidHeap.ordered_end()) {
        for(int k = 0; k < (*bidHeapIter).mass; ++k) {
            resShort.push_back( (*bidHeapIter).loss);
        }
        ++bidHeapIter;
    }
    std::sort(resShort.begin(), resShort.end());
    
    auto wHeapIter = wHeap.ordered_begin();
    while(wHeapIter != wHeap.ordered_end()) {
        for(int k = 0; k < (*wHeapIter).mass; ++k) {
            resW.push_back( (*wHeapIter).loss);
        }
        ++wHeapIter;
    }
    std::sort(resW.begin(), resW.end());
    assert(resW.size() >= resShort.size());
    for(size_t k = 0; k < resShort.size(); ++k) {
        assert( fabs( resShort[k] - resW[k] ) < 0.001 );
    }
    return resShort;
}
 // ObjectBidTable

void ObjectBidTable::sanityCheck() const
{
#ifdef DEBUG_AUCTION_KDTREE
#endif
}

void ObjectBidTable::clear(void)
{
    bidHeap.clear();
    totalMass = 0;
    sanityCheck();
}

void ObjectBidTable::insert(const ObjectBid& bid)
{
    assert(bid.mass > 0);
    assert(bid.value > 0.0);
    assert(capacity > 0);
    sanityCheck();
    //std::cout << "HERE: " << capacity << ", " << totalMass << std::endl <<  "; " << bidList.size() << std::endl;
    // if bid has no chance, reject it
    if (totalMass == capacity and bidHeap.top().value >= bid.value)
        return;
    bidHeap.push(bid);
    totalMass += bid.mass;
    while(true) {
        ObjectBid worstBid = bidHeap.top();
        if ( totalMass - worstBid.mass >= capacity ) {
            totalMass -= worstBid.mass;
            bidHeap.pop();
        } else {
            break;
        }
    }
    if ( totalMass > capacity ) {
        bidHeap.top().mass -= ( totalMass - capacity);
        totalMass = capacity;
    }
    sanityCheck();
}



BidTableKeeperArray::BidTableKeeperArray(size_t _numBidders, size_t _numObjects) :
    numBidders(_numBidders),
    numObjects(_numObjects),
    assignedObjectsBidTables(boost::extents[_numBidders][_numObjects]),
    unassignedObjectsBidTables(boost::extents[_numBidders][_numObjects])
{
    assert(numBidders > 0);
    assert(numObjects > 0);
    for(size_t ownerIdx = 0; ownerIdx < numBidders; ++ownerIdx) {
        for(size_t objectIdx = 0; objectIdx < numObjects; ++objectIdx) {
            assignedObjectsBidTables[ownerIdx][objectIdx] = nullptr;
            unassignedObjectsBidTables[ownerIdx][objectIdx] = nullptr;
        }
    }
}

BidTableKeeperArray::~BidTableKeeperArray()
{
    for(size_t ownerIdx = 0; ownerIdx < numBidders; ++ownerIdx) {
        for(size_t objectIdx = 0; objectIdx < numObjects; ++objectIdx) {
            delete assignedObjectsBidTables[ownerIdx][objectIdx];
            delete unassignedObjectsBidTables[ownerIdx][objectIdx];
        }
    }
}

ObjectBidTable* BidTableKeeperArray::getSliceBidTable(const SliceIdx& slice)
{
    ObjectBidTable* bidTable = ( slice.isAssigned ) ? assignedObjectsBidTables[slice.ownerIdx][slice.objectIdx] : unassignedObjectsBidTables[slice.ownerIdx][slice.objectIdx];
    assert(bidTable != nullptr);
    return bidTable;
}

void BidTableKeeperArray::setCapacity(const SliceIdx& slice, MassType newMass)
{
    auto& bidTable = ( slice.isAssigned ) ? assignedObjectsBidTables[slice.ownerIdx][slice.objectIdx] : unassignedObjectsBidTables[slice.ownerIdx][slice.objectIdx];
    if (newMass == 0) {
        delete bidTable;
        bidTable = nullptr;
    } else {
        if (bidTable == nullptr) {
            bidTable = new ObjectBidTable(newMass);
        } else {
            bidTable->clear();
            bidTable->capacity = newMass;
        }
    }
}

void BidTableKeeperArray::submitBid(const SliceIdx& slice, const ObjectBid& bid)
{
    getSliceBidTable(slice)->insert(bid);
}
 // ---- end of ObjectBidTable

// debug output operators
std::ostream& operator<<(std::ostream& output, const Bid& bid)
{
    output << "bid: bidder = " << bid.bidderIdx << ", slice = " << bid.slice << ", mass = " << bid.mass;
    output << ", loss = " << bid.loss;
    return output;
}

std::ostream& operator<<(std::ostream& output, BidTable& bidTable) 
{
    output << "Bid table, capacity = " << bidTable.capacity << " , bidHeapAccumulatedMass =  " << bidTable.bidHeapAccumulatedMass << ", wHeapAccumulatedMass= " << bidTable.wHeapAccumulatedMass << std::endl;
    for(auto& bid : bidTable.bidHeap) {
        output << bid << std::endl;
    }
    output << "end of bidHeap" << std::endl << "wHeap:" << std::endl;
    for(auto& bid : bidTable.wHeap) {
        output << bid << std::endl;
    }
    output << "end of wHeap" << std::endl;
    output << "objects in table:" << bidTable.objectSet << std::endl;
    output << "w = " << bidTable.getW() << ", isFull = " << bidTable.isFull() << std::endl;
    output << "end of BidTable" << std::endl;
    return output;
}

std::ostream& operator<<(std::ostream& output, const ObjectBid& bid) 
{
    output << "object bid: bidder = " << bid.bidderIdx <<  ", mass = " << bid.mass << ", value = " <<  bid.value;
    return output;
}

std::ostream& operator<<(std::ostream& output, const ObjectBidTable& bidTable) 
{
    output << "Object bid table, totalMass =  " << bidTable.totalMass << ", capacity = " << bidTable.capacity << std::endl;
    auto bidIter = bidTable.bidHeap.ordered_begin();
    while(bidIter != bidTable.bidHeap.ordered_end()) {
        output << *bidIter++ << std::endl;
    }
    output << "end of object bid table" << std::endl;
    return output;
}

std::ostream& operator<<(std::ostream& output, BidHeap& h)
{
    output << "BidHeap:" << std::endl;
    auto priceHeapIter = h.ordered_begin();
    while(priceHeapIter != h.ordered_end() ) {
        output << *priceHeapIter++ << std::endl;
    }
    output << "end of BidderHeap:" << std::endl;
    return output;
}

} // end of namespace
