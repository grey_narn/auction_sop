#include <chrono>
#include <iomanip>
#include "auction_kdtree_gs.h"

//#define PRINT_DETAILED_TIMING
//#define PRINT_FLUSH_STATS
//#define GATHER_STAT_KDTREE

namespace auction_sop {

// debug output
//
std::string toStringSeparated(long int val)
{
    std::string result = std::to_string(val);
    int insPos = result.length() - 3;
    while (insPos > 0) {
        result.insert(insPos, ",");
        insPos -= 3;
    }
    return result;
}

std::ostream& operator<<(std::ostream& output, AuctionKdtreeRunnerGS& auction)
{
    output << "****************************************************" << std::endl;
    output << "numNormalBidders : " << auction.numNormalBidders << ", numBidders : " << auction.numBidders << ", numObjects : " << auction.numObjects << ", numNormalObjects : " << auction.numNormalObjects << std::endl;
    output << "bidders:" << std::endl;
    for(size_t bi = 0 ; bi <  auction.numBidders; ++ bi) {
        output << bi << " : " << auction.allBidders[bi]  << std::endl;
    }
    output << std::endl << "objects:" << std::endl;
    for(size_t bo = 0 ; bo <  auction.numObjects; ++ bo) {
        output << bo << " : " << auction.allObjects[bo]  << std::endl;
    }
    output << std::endl;

    for(size_t bidderIdx = 0; bidderIdx < auction.numBidders; ++bidderIdx)  {
        if (auction.slicesAssignedToBidder[bidderIdx].size() > 0) {
            output << "slices assigned to bidder " << bidderIdx << ": ";
            for(const auto& s : auction.slicesAssignedToBidder[bidderIdx]) {
                output << s << ", mass = " <<  auction.getSliceMass(s) << ", price = " << auction.getSlicePrice(s) << "; ";
            }
            output << std::endl;
        }
    }

    output << "objectSlices: " << std::endl;
    for(size_t objIdx = 0; objIdx < auction.numObjects; ++objIdx) {
        if (not auction.objectSlices[objIdx].empty()) {
            output << "slices of object " << objIdx << " : " << std::endl;
            output << auction.objectSlices[objIdx] << std::endl;
        }
    }
    output << "end of objectSlices" << std::endl;

    output << "globalSlices: " << auction.globalSlices << std::endl;

    //unassigned bidders:
#ifndef KEEP_UNASSIGNED_ORDERED
    output << std::endl << "Unassigned bidders: " << auction.unassignedBidders << std::endl;
#endif

    output << "localSliceIdxToIterMap" << std::endl;
    for(const auto iterPair : auction.localSliceIdxToIterMap) {
        output << iterPair.first << " <--> " << *iterPair.second << std::endl;
    }
    output << "end of localSliceIdxToIterMap" << std::endl;

    output << "globalSliceIdxToIterMap" << std::endl;
    for(const auto iterPair : auction.globalSliceIdxToIterMap) {
        output << iterPair.first << " <--> " << *iterPair.second << std::endl;
    }
    output << "end of globalSliceIdxToIterMap" << std::endl;


    output << "****************************************************" << std::endl;
    return output;
}

double AuctionKdtreeRunnerGS::getDistToBidder(const size_t bidderIdx, size_t objectIdx) const
{
    if ( objectIdx > numNormalObjects and bidderIdx > numNormalBidders ) {
        return 0.0;
    }
    const auto& bidder = allBidders[bidderIdx];
    const auto& object = allObjects[objectIdx];
    if ( objectIdx >= numNormalObjects )  {
        return bidder.getPersistence();
    } else if ( bidderIdx >= numNormalBidders ) {
        return object.getPersistence();
    } else {
        return std::max(fabs(bidder.getRealX() - object.getRealX()), fabs(bidder.getRealY() - object.getRealY()));
    }
}

double AuctionKdtreeRunnerGS::getLossForBiddder(const size_t bidderIdx, const PointSlice& slice) const
{
    return getDistToBidder(bidderIdx, slice.getObjectIdx()) + slice.getPrice();
}

AuctionKdtreeRunnerGS::AuctionKdtreeRunnerGS(std::vector<DiagramPoint>& setAPoints,
                                             std::vector<DiagramPoint>& setBPoints,
                                             double _wassersteinPower,
                                             double _delta,
                                             const std::string fnameStat,
                                             const double _initialEpsilon,
                                             const double _epsFactor) :
    statKeeper(fnameStat),
    kdtree(nullptr),
    numNormalBidders(setAPoints.size()),
    numBidders(setAPoints.size() + 1),
    numNormalObjects(setBPoints.size()),
    numObjects(setBPoints.size() + 1),
    wassersteinPower(_wassersteinPower),
    maxRelError(_delta),
    initialEpsilon(_initialEpsilon),
    epsilonCommonRatio(_epsFactor == 0.0 ? 5.0 : _epsFactor),
    slicesAssignedToBidder(setAPoints.size() + 1)
{
    //std::cout << "entered AuctionKdtreeRunner ctor" << std::endl;

    allBidders.reserve(numNormalBidders + 1);
    allObjects.reserve(numNormalObjects + 1);
    persistences.reserve(numNormalObjects + 1);

    totalBidderMass = 0;
    for(size_t normalBidderIdx = 0; normalBidderIdx < numNormalBidders; ++normalBidderIdx) {
        allBidders.push_back(setAPoints[normalBidderIdx]);
        totalBidderMass += allBidders.back().getMass();
    }

    totalObjectMass = 0;
    for(size_t objIdx = 0; objIdx < numNormalObjects; ++objIdx) {
        allObjects.push_back(setBPoints[objIdx]);
        totalObjectMass += allObjects.back().getMass();
    }
    // add one diagonal bidder and one diagonal object
    diagBidderMass = totalObjectMass;
    diagObjectMass = totalBidderMass;
    allBidders.emplace_back( 0.0, 0.0, diagBidderMass, DiagramPoint::DIAG );
    allObjects.emplace_back( 0.0, 0.0, diagObjectMass, DiagramPoint::DIAG );

    totalMass = totalBidderMass + totalObjectMass;

    //std::cout << "vectors copied" << std::endl;
    // reset ids for indices in vector.
    // this means that bidders and objects will have the same id,
    // so it's no longer unique!
    // must not be a problem, for bidders and objects play totally different
    // roles
    dnnBidders.reserve(numNormalBidders);
    for(size_t bidderIdx = 0; bidderIdx < numNormalBidders; ++bidderIdx) {
            // index of items is id of dnn-point
            auto& bidder = allBidders[bidderIdx];
            DnnPoint p(bidderIdx);
            p[0] = bidder.getRealX();
            p[1] = bidder.getRealY();
            dnnBidders.push_back(p);
    }
    //std::cout << "dnnBidders OK" << std::endl;

    objectSlices.reserve(numObjects);
    localSliceIdxToIterMap.reserve(3 * numObjects);
    globalSliceIdxToIterMap.reserve(3 * numObjects);

    minPersistence = std::numeric_limits<double>::max();
    for(size_t objIdx = 0; objIdx < numNormalObjects; ++objIdx) {
        SliceIdx newSliceIdx { NO_OWNER_IDX, objIdx, false };
        double persistence = allObjects[objIdx].getPersistence();
        persistences.push_back(persistence);
        if ( persistence < minPersistence ) {
            minPersistence = persistence;
        }
        PointSlice newSlice {newSliceIdx, allObjects[objIdx].getMass(), 0.0, persistence };
        objectSlices.push_back(PointSliceHeapAsc({ newSlice }));
        localSliceIdxToIterMap[newSliceIdx] = objectSlices.back().begin();
        auto globInsRes = globalSlices.insert(newSlice);
        assert(globInsRes.second);
        globalSliceIdxToIterMap[newSliceIdx] = globInsRes.first;
    }

    // add diagonal slice
    {
        SliceIdx diagSliceIdx { NO_OWNER_IDX, numNormalObjects, false };
        PointSlice diagSlice {diagSliceIdx, allObjects.back().getMass(), 0.0, 0.0};
        persistences.push_back(0.0);
        objectSlices.push_back(PointSliceHeapAsc({ diagSlice }));
        localSliceIdxToIterMap[diagSliceIdx] = objectSlices.back().begin();
        auto globInsRes = globalSlices.insert(diagSlice);
        assert(globInsRes.second);
        globalSliceIdxToIterMap[diagSliceIdx] = globInsRes.first;
    }

    //std::cout << "objectSlices OK" << std::endl;

    for(size_t bidder_idx = 0; bidder_idx < allBidders.size(); ++bidder_idx) {
#ifdef KEEP_UNASSIGNED_ORDERED
        unassignedBidders.insert(std::make_pair(bidder_idx, allBidders[bidder_idx]), allBidders[bidder_idx].getMass());
#else
        unassignedBidders.insert(bidder_idx, allBidders[bidder_idx].getMass());
#endif
    }
    //std::cout << "unassignedBidders OK" << std::endl;


    initKDTree();
    //std::cout << "kdtree OK" << std::endl;

    maxVal = 3.0 * getFurthestDistance3Approx(allBidders, allObjects);

    for(size_t bidderIdx = 0; bidderIdx < numNormalBidders; ++bidderIdx) {
        minPersistence = std::min( minPersistence, allBidders[bidderIdx].getPersistence() );
    }

    sanityCheck();
}

void AuctionKdtreeRunnerGS::initKDTree()
{
    assert(dnnPoints.size() == 0);
    // store normal items in kd-tree
    for(size_t objIdx = 0; objIdx < numNormalObjects; ++objIdx) {
            // index of items is id of dnn-point
            auto& obj = allObjects[objIdx];
            DnnPoint p(objIdx);
            p[0] = obj.getRealX();
            p[1] = obj.getRealY();
            dnnPoints.push_back(p);
    }
    assert(dnnPoints.size() == numNormalObjects );
    for(size_t i = 0; i < dnnPoints.size(); ++i) {
        dnnPointHandles.push_back(DnnTraits::handle(dnnPoints[i]));
    }
    DnnTraits traits;
    //std::cout << "kdtree: " << dnnPointHandles.size() << " points" << std::endl;
    kdtree = new dnn::KDTree<DnnTraits>(traits, dnnPointHandles, wassersteinPower);
}

void AuctionKdtreeRunnerGS::initGlobalHeap()
{
    MassType diagToDiagMass { 0 };
    int numDiagToDiagSlices { 0 };
    int numDiagSlice { static_cast<int>(objectSlices.back().size()) };
    auto oldSlices = globalSlices;
    std::unordered_set<double> diagPrices, diagToDiagPrices;
    globalSlices.clear();
    globalSliceIdxToIterMap.clear();
    globalSliceIdxToIterMap.reserve(oldSlices.size());
    for(auto oldSlice : oldSlices) {
#ifdef PRINT_FLUSH_STATS
        // gather stats
        bool isDiagToDiag = oldSlice.getOwnerIdx() >= numNormalBidders;
        diagPrices.insert(oldSlice.getPrice());
        if (isDiagToDiag) {
            diagToDiagMass += oldSlice.getMass();
            numDiagToDiagSlices++;
            diagToDiagPrices.insert(oldSlice.getPrice());
        }
#endif
        oldSlice.setIsAssigned(false);
        auto insRes = globalSlices.insert(oldSlice);
        assert(insRes.second);
        globalSliceIdxToIterMap[oldSlice.getSliceIdx()] = insRes.first;
    }

#ifdef PRINT_FLUSH_STATS
    std::cout << " Diag slices: " << numDiagSlice << " diagonal slices , " << " total diag mass " << diagObjectMass << ", diagToDiagMass = " << diagToDiagMass << ", diagToDiag slices = " << numDiagToDiagSlices << std::endl;
    std::cout << " diagPrices: " << diagPrices.size() << ", diagToDiagPrices = " << diagToDiagPrices.size() << std::endl;
#endif
}

void AuctionKdtreeRunnerGS::addBidToProjectionPreimage(size_t bidderIdx, MassType unassignedMass, BidTable& bidTable)
{
    assert(bidderIdx < numNormalBidders);
    double distToProj = allBidders[bidderIdx].getPersistence(); // object and bidder have the same index <-> one is diagonal projection of the other one
    auto& diagSlices = objectSlices.back();

    MassType inspectedMass { 0 };

    //std::cout << "entered addBidToProjectionPreimage, bidderIdx = " << bidderIdx << ", unassignedMass = " << unassignedMass << std::endl;
    for(auto sliceIter = diagSlices.begin(); sliceIter != diagSlices.end(); ++sliceIter) {
        auto& slice = *sliceIter;
        double sliceLoss = distToProj + slice.price;
        if ( sliceLoss >= bidTable.getW()) {
            break;
        }

        if (slice.getIsAssigned() and slice.getOwnerIdx() == bidderIdx) {
            continue;
        }

        MassType availableMass = slice.getMass();
        inspectedMass += availableMass;
        MassType bidMass = std::min(unassignedMass, availableMass);
        bidTable.insert( Bid(bidderIdx, slice, bidMass, sliceLoss, availableMass));
        if ( inspectedMass > unassignedMass ) {
            break;
        }
    }
}


void AuctionKdtreeRunnerGS::runAuctionPhase(void)
{
    //std::cout << "started runAuctionPhase" << std::endl;
    while(not unassignedBidders.empty()) {
#ifdef GATHER_STAT_KDTREE
        statKeeper.unassignedBidders = unassignedBidders.size();
        statKeeper.unassignedMass = 0;
        statKeeper.unassignedDiagMass= 0;
        statKeeper.unassignedDiagBidders = 0;
        for(auto unassignedPair : unassignedBidders) {
            statKeeper.unassignedMass += unassignedPair.second;
#ifdef KEEP_UNASSIGNED_ORDERED
            if ( static_cast<size_t>(unassignedPair.first.first) >= numNormalBidders) {
#else
            if ( static_cast<size_t>(unassignedPair.first) >= numNormalBidders) {
#endif
                statKeeper.unassignedDiagMass += unassignedPair.second;
                statKeeper.unassignedDiagBidders++;
            }
        }
#endif

        for(auto unassignedPair : unassignedBidders) {
            //std::cout << "submitBid for" << unassignedPair.first << ", mass = " << unassignedPair.second << std::endl;
            // process diagonal bidder after everyone else
            if (unassignedBidders.size() > 1 and unassignedPair.first >= static_cast<long int>(numNormalBidders)) {
                continue;
            }
#ifdef KEEP_UNASSIGNED_ORDERED
            submitBids(std::make_pair(unassignedPair.first.first, unassignedPair.second));
#else
            submitBids(unassignedPair);
#endif
            numRounds++;
            //if (numRounds % 100 == 0 or numRounds > 300000 ) {
                //std::cout << "round " << numRounds << ", unassignedMass = " << unassignedBidders.weighted_size() << ", unassignedBidders " << unassignedBidders.size() << std::endl << allBidders[unassignedPair.first] << std::endl;
            //}
            //std::cout << diagSlices << std::endl;
            //std::cout << "round ok" << std::endl;
            break;
        }
        sanityCheck();
#ifdef GATHER_STAT_KDTREE
        statKeeper.totalNumDiagSlices = 0;
        statKeeper.totalNumNormalSlices = 0;
        statKeeper.totalNumDiagSlices = objectSlices.back().size();
        for(size_t objIdx = 0; objIdx < numNormalObjects; ++objIdx) {
            statKeeper.totalNumNormalSlices += objectSlices[objIdx].size();
        }
        statKeeper.totalNumSlices = statKeeper.totalNumDiagSlices + statKeeper.totalNumNormalSlices;
        statKeeper.printStats();
        statKeeper.resetForPhase();
#endif
    }
}

bool AuctionKdtreeRunnerGS::sliceExists(const SliceIdx& sliceIdx) const
{
    return localSliceIdxToIterMap.find(sliceIdx) != localSliceIdxToIterMap.end();
}

PointSliceIter AuctionKdtreeRunnerGS::getLocalSliceIter(const SliceIdx& sliceIdx)
{
    assert(sliceExists(sliceIdx));
    return localSliceIdxToIterMap[sliceIdx];
}

PointSliceIter AuctionKdtreeRunnerGS::getLocalSliceIter(const PointSlice& slice)
{
    return getLocalSliceIter(slice.getSliceIdx());
}

PointSliceIter AuctionKdtreeRunnerGS::getGlobalSliceIter(const SliceIdx& sliceIdx)
{
    assert(sliceExists(sliceIdx));
    return globalSliceIdxToIterMap[sliceIdx];
}

PointSliceIter AuctionKdtreeRunnerGS::getGlobalSliceIter(const PointSlice& slice)
{
    return getGlobalSliceIter(slice.getSliceIdx());
}

void AuctionKdtreeRunnerGS::createAssignedSlice(const size_t bidderIdx, const size_t objectIdx, const MassType mass, const double price)
{
    SliceIdx newSliceIdx { bidderIdx, objectIdx, true };
    slicesAssignedToBidder[bidderIdx].insert(newSliceIdx);
    // insert into local heap of object
    auto localInsRes = objectSlices[objectIdx].insert(PointSlice(newSliceIdx, mass, price, persistences[objectIdx]));
    assert(localInsRes.second);
    localSliceIdxToIterMap[newSliceIdx] = localInsRes.first;
    // insert into global heap
    auto globalInsRes = globalSlices.insert(PointSlice(newSliceIdx, mass, price, persistences[objectIdx]));
    assert(globalInsRes.second);
    globalSliceIdxToIterMap[newSliceIdx] = globalInsRes.first;
}

bool AuctionKdtreeRunnerGS::isDiagonal(const PointSlice& slice) const
{
    return slice.getObjectIdx() >= numNormalObjects;
}

bool AuctionKdtreeRunnerGS::isDiagonal(const SliceIdx& slice) const
{
    return slice.getObjectIdx() >= numNormalObjects;
}


void AuctionKdtreeRunnerGS::eraseSlice(PointSlice slice, bool changeUnassigned)
{
    auto globalIter = getGlobalSliceIter(slice);
    globalSlices.erase(globalIter);
    globalSliceIdxToIterMap.erase(slice.getSliceIdx());

    auto iter = getLocalSliceIter(slice);
    objectSlices[slice.getObjectIdx()].erase(iter);
    localSliceIdxToIterMap.erase(slice.getSliceIdx());

    if (changeUnassigned and slice.getIsAssigned()) {
#ifdef KEEP_UNASSIGNED_ORDERED
        unassignedBidders.insert(std::make_pair(slice.getOwnerIdx(), allBidders[slice.getOwnerIdx()]),  slice.getMass());
#else
        unassignedBidders.insert(slice.getOwnerIdx(), slice.getMass());
#endif
        slicesAssignedToBidder[slice.getOwnerIdx()].erase(slice.getSliceIdx());
    }

    if (not isDiagonal(slice)) {
        normalObjectsWithChangedPrices.insert(slice.getObjectIdx());
    }
}

void AuctionKdtreeRunnerGS::checkPerfectMatching()
{
#ifdef DEBUG_AUCTION_KDTREE_GS
    // check slicesAssignedToBidder
    for(size_t bidderIdx = 0; bidderIdx < numBidders; ++bidderIdx) {
        MassType totalMass = 0;
        for(const auto s : slicesAssignedToBidder[bidderIdx]) {
            totalMass += getSliceMass(s);
        }
        if (totalMass != allBidders[bidderIdx].getMass()) {
            std::cerr << "Matching is not perfect!" << std::endl;
            std::cerr << "totalMass: " << totalMass << ", bidder mass : " << allBidders[bidderIdx].getMass() << std::endl;
            std::cerr << bidderIdx << ", " << allBidders[bidderIdx] << std::endl;
            throw 1;
        }
    }

    for(size_t objectIdx = 0; objectIdx < numObjects; ++objectIdx) {
        MassType totalMass = 0;
        for(const auto s : objectSlices[objectIdx] ) {
            if (not s.getIsAssigned() ) {
                std::cerr << "Matching not perfect, object slice " << s << std::endl;
                assert(false);
                throw "qq";
            } else {
                totalMass += s.getMass();
            }
        }
        assert( totalMass <= allObjects[objectIdx].getMass() );
    }

#endif
}

void AuctionKdtreeRunnerGS::flushAssignment()
{

    //std::cout << "entered flushAssignment" << std::endl;
    //std::cout << "unassignedBidders: " << unassignedBidders << std::endl;

    checkPerfectMatching();

    assert(unassignedBidders.empty());
    // all bidders are unassigned:
    auto numOfSlices = localSliceIdxToIterMap.size();
    localSliceIdxToIterMap.clear();
    localSliceIdxToIterMap.reserve(numOfSlices);

    globalSliceIdxToIterMap.clear();
    globalSliceIdxToIterMap.reserve(numOfSlices);

    for(size_t bidderIdx = 0; bidderIdx < numBidders; ++bidderIdx) {
        // make bidder unassigned
#ifdef KEEP_UNASSIGNED_ORDERED
        unassignedBidders.insert(std::make_pair(bidderIdx, allBidders[bidderIdx]), allBidders[bidderIdx].getMass());
#else
        unassignedBidders.insert(bidderIdx, allBidders[bidderIdx].getMass());
#endif
    }
    //std::cout << "after flush, unassignedBidders: " << unassignedBidders.weighted_size() << std::endl;

    for(size_t objectIdx = 0; objectIdx < numObjects; ++objectIdx) {
        PointSliceHeapAsc oldPointSliceHeap = objectSlices[objectIdx];
        objectSlices[objectIdx].clear();
        for(auto slice : oldPointSliceHeap) {
            assert(slice.sliceIdx.isAssigned);
            slice.sliceIdx.isAssigned = false;
            auto insRes = objectSlices[objectIdx].insert(slice);
            assert(insRes.second);
            assert(slice.getMass() > 0);
            auto iterMapInsRes = localSliceIdxToIterMap.insert(std::make_pair(slice.sliceIdx, insRes.first));
            if (not iterMapInsRes.second) {
                std::cerr << "sliceIdx = " << slice.sliceIdx << std::endl;
            }
            assert(iterMapInsRes.second);
        }
    }

    //std::cout << "number of point slices: " << numOfPointSlices << ", numOfDiagSlices = " << numOfDiagSlices << ", iterMap.size = " << localSliceIdxToIterMap.size() << std::endl;

    for(auto& sliceSet : slicesAssignedToBidder) {
        sliceSet.clear();
    }

    initGlobalHeap();

    //std::cout << "after flush: " << *this << std::endl;
}

double AuctionKdtreeRunnerGS::getSlicePrice(const SliceIdx& sliceIdx)
{
    return getLocalSliceIter(sliceIdx)->getPrice();
}

void AuctionKdtreeRunnerGS::stealMassFromSlice(const PointSlice& slice, MassType delta)
{
    assert(sliceExists(slice.sliceIdx));
    if (delta < slice.getMass()) {
        changeSliceMass(slice.getSliceIdx(), -delta);
        if (slice.getIsAssigned()) {
#ifdef KEEP_UNASSIGNED_ORDERED
            unassignedBidders.insert(std::make_pair(slice.getOwnerIdx(), allBidders[slice.getOwnerIdx()]), delta);
#else
            unassignedBidders.insert(slice.getOwnerIdx(), delta);
#endif
        }
    } else {
        eraseSlice(slice, true);
    }
}

void AuctionKdtreeRunnerGS::prepareBidTableDiagonal(size_t bidderIdx, MassType unassignedMass, std::vector<Bid>& bidTable, double& w, MyMultiset<size_t> objSet)
{
    bidTable.clear();
    //std::cout << "entered prepareBidTableDiagonal, bidderIdx = " << bidderIdx << ", unassignedMass = " << unassignedMass << std::endl;
    auto& slices = globalSlices; // object and bidder have the same index <-> one is diagonal projection of the other one
    auto sliceIter = slices.begin();
    while(sliceIter != slices.end() and sliceIter->getOwnerIdx() == bidderIdx and sliceIter->getIsAssigned()) {
        ++sliceIter;
    }
    //std::cout << "unassignedMass = " << unassignedMass << std::endl;
    //std::cout << "objset: " << objSet << std::endl;
    // accumulate the best objects
    while( sliceIter != slices.end() ) {
        PointSlice bidSlice = *sliceIter;
        double bidLoss = getLossForBiddder(bidderIdx, bidSlice);
        MassType availableMass = sliceIter->getMass();
        objSet.insert(bidSlice.getObjectIdx(), availableMass);
        //std::cout << "next best : " << bidSlice << std::endl;
        // move slice iterator forward, skip goods assigned to the same bidder
        do {
            ++sliceIter;
        } while(sliceIter != slices.end() and sliceIter->getOwnerIdx() == bidderIdx and sliceIter->getIsAssigned());

        if (unassignedMass > 0) {
            MassType bidMass = std::min(unassignedMass, availableMass);
            unassignedMass -= availableMass;
            //std::cout << "inserted bid, unassignedMass = "  << unassignedMass << std::endl;
            assert(bidMass > 0);
            bidTable.push_back( Bid(bidderIdx, bidSlice, bidMass, bidLoss, availableMass));
            if (unassignedMass < 0 and objSet.size() > 1) {
                w = bidLoss;
                return;
            }
        } else if (objSet.size() > 1) {
            w = bidLoss;
            return;
        }
    }
    //std::cout << "exit prepareBidTableDiagonal " << std::endl;
}

void AuctionKdtreeRunnerGS::submitBids(MyMultiset<MassType>::PairType unassignedPair)
{
    //MassType unassignedBefore = unassignedBidders.weighted_size();
    sanityCheck();
    size_t bidderIdx = unassignedPair.first;
    MassType unassignedMass = unassignedPair.second;
    //std::cout << "current bidder: " << bidderIdx << ", wants " << unassignedMass << std::endl;
    //std::cout << "entered submitBids for bidderIdx = " << bidderIdx << ", unassignedMass = " << unassignedMass << std::endl;
    assert(unassignedMass > 0);
    assert(bidderIdx >= 0);
    assert(bidderIdx < numBidders);

    MyMultiset<size_t> assignedObjectMultiset;
    for(const auto& s : slicesAssignedToBidder[bidderIdx]) {
        assignedObjectMultiset.insert(s.objectIdx, getSliceMass(s));
    }

    assert(static_cast<int>(assignedObjectMultiset.weighted_size()) + unassignedMass == allBidders[bidderIdx].getMass());
    BidTable bidTable { unassignedMass, assignedObjectMultiset };
    std::vector<Bid> bidVector;
    double w;

    if (bidderIdx < numNormalBidders) {
        // for normal objects we use kdtree
        //std::cout << "for normal objects we use kdtree" << std::endl;
        kdtree->findSOP(DnnTraits::handle(dnnBidders[bidderIdx]), wassersteinPower, &bidTable, &objectSlices);
        //std::cout << "kdtree->findSOP done, bidTable: " << bidTable << std::endl;
        addBidToProjectionPreimage(bidderIdx, unassignedMass, bidTable);
        // submit bids for objects we're assigned to
    } else {
        prepareBidTableDiagonal(bidderIdx, unassignedMass, bidVector, w, assignedObjectMultiset);
        //prepareBidTableDiagonal(bidderIdx, unassignedMass, bidTable);
    }

#ifdef DEBUG_AUCTION_KDTREE_GS
    //std::cout << "debug for " << bidderIdx << std::endl;
    std::vector<double> debugTablelosss;
    double tableW;
    if (bidderIdx < numNormalBidders) {
        tableW = bidTable.getW();
        debugTablelosss = bidTable.getDebuglosss();
    } else {
        tableW = w;
        for(const auto& b : bidVector) {
            for(int k = 0; k < b.mass; ++k) {
                debugTablelosss.push_back(b.loss);
            }
        }
    }
    std::vector<PointSlice> debugSlices;
    for(auto slices : objectSlices) {
        for(PointSlice slice : slices) {
            if (slice.getIsAssigned() and slice.getOwnerIdx() == bidderIdx) {
                continue;
            }
            slice.price += getDistToBidder(bidderIdx, slice.getObjectIdx());
            debugSlices.push_back(slice);
        }
    }

    //std::cout << "debugSlices : " << std::endl;
    //for(const auto s : debugSlices) {
        //std::cout << s << std::endl;
    //}
    //std::cout << "end of debugSlices : " << std::endl;

    std::sort(debugSlices.begin(), debugSlices.end(), CompPointSlicesByPriceLess());
    std::vector<double> debuglosss;
    std::vector<size_t> debugObjects;
    for(auto& s : debugSlices) {
        for(int k = 0; k < s.mass; ++k) {
            debuglosss.push_back(s.price);
            debugObjects.push_back(s.getObjectIdx());
        }
    }

    std::unordered_set<size_t> debugObjSet;
    size_t k;

    for(const auto& s : slicesAssignedToBidder[bidderIdx]) {
        debugObjSet.insert(s.objectIdx);
    }


    for(k = 0; k < static_cast<decltype(k)>(unassignedMass); ++k) {
        debugObjSet.insert(debugObjects[k]);
        if ( fabs( debuglosss[k] - debugTablelosss[k] ) > 0.0001) {

            std::cout << "bidderIdx = " << bidderIdx << std::endl;

            std::cout << "debugTablelosss: " << std::endl;
            for(auto p : debugTablelosss) {
                std::cout << p << ", ";
            }
            std::cout << std::endl;
            std::cout << "debuglosss: " << std::endl;
            for(auto p : debuglosss) {
                std::cout << p << ", ";
            }
            std::cout << std::endl;
            std::cout << "debugObjects: " << std::endl;
            for(auto p : debugObjects) {
                std::cout << p << ", ";
            }
            std::cerr << *this << std::endl;
        }
        assert( fabs( debuglosss[k] - debugTablelosss[k] ) < 0.0001);
    }

    while(debugObjSet.size() == 1) {
        debugObjSet.insert(debugObjects[k]);
        if (debugObjSet.size() == 1)
            k++;
    }
    double debSimpleW = debuglosss[k];
    if ( fabs( debSimpleW - tableW) >= 0.0001 ) {
        std::cerr << "tableW = " << tableW << ", naive w = " << debSimpleW << std::endl;
        std::cout << "debugTablelosss: " << std::endl;
        for(auto p : debugTablelosss) {
            std::cout << p << ", ";
        }
        std::cout << std::endl;
        std::cout << "debuglosss: " << std::endl;
        for(auto p : debuglosss) {
            std::cout << p << ", ";
        }
        std::cout << std::endl;
        std::cout << "debugObjects: " << std::endl;
        for(auto p : debugObjects) {
            std::cout << p << ", ";
        }
        std::cerr << *this << std::endl;
        std::cerr << getDistToBidder(10, 0) << std::endl;
        assert( false );
    }
    //std::cout << "bids OK" << std::endl;
    //
#endif

    //MassType unassignedDelta = 0;
    //for(const auto bid : bidTable) {
        //if (not bid.slice.getIsAssigned())
            //unassignedDelta += bid.mass;
    //}

    if (bidderIdx < numNormalBidders) {
        reassignSlices(bidderIdx, unassignedMass, bidTable);
    } else {
        reassignSlices(bidderIdx, unassignedMass, bidVector, w);
    }
    //if ( unassignedBefore - unassignedDelta != static_cast<int>(unassignedBidders.weighted_size()) ) {
        //std::cerr << "error while processing bid for bidderIdx = " << bidderIdx << ", bidTable " << bidTable << std::endl;
        //std::cerr << "Unassignmed before: " << unassignedBefore << ", after " << unassignedBidders.weighted_size() << ", bid mass for unassigned " << unassignedDelta << std::endl;
        //throw 1;
    //}
    //assert( unassignedBefore - unassignedDelta == static_cast<int>(unassignedBidders.weighted_size()) );
    //std::cout << "Unassigned mass after: " << unassignedBidders.weighted_size() << std::endl;
}

void AuctionKdtreeRunnerGS::changeSliceMass(const SliceIdx& sliceIdx, MassType delta)
{
    getLocalSliceIter(sliceIdx)->increaseMassBy(delta);
    getGlobalSliceIter(sliceIdx)->increaseMassBy(delta);
}

void AuctionKdtreeRunnerGS::reassignSlices(const size_t bidderIdx, const MassType unassignedMass, std::vector<Bid>& bidTable, double w)
{
    assert( w < std::numeric_limits<double>::max() );

    sanityCheck();
    std::unordered_set<size_t> objectsWithBids;
    objectsWithBids.reserve( slicesAssignedToBidder[bidderIdx].size() + bidTable.size() );
    normalObjectsWithChangedPrices.clear();
    normalObjectsWithChangedPrices.reserve( slicesAssignedToBidder[bidderIdx].size() + bidTable.size() );

    // in Gauss-Seidel version we just increase the prices of the slices
    // to which we've been assigned; note that the objects modified here
    // are disjoint from the objects to whose slices we bid
    for(auto& assignedSlice : slicesAssignedToBidder[bidderIdx]) {
        size_t objectIdx = assignedSlice.getObjectIdx();
        double newPrice = w - getDistToBidder(bidderIdx, objectIdx) + epsilon;
        assert( newPrice >= getSlicePrice(assignedSlice) );
        auto oldSliceIter =  getLocalSliceIter(assignedSlice);
        MassType newMass = oldSliceIter->getMass();
        eraseSlice(*oldSliceIter, false); // second argument: do not change unassignedBidders
        createAssignedSlice(bidderIdx, objectIdx, newMass, newPrice);
    }

    // in Gauss-Seidel version we automatically get the objects we want,
    // since in each iteration of the loop we are the only bidder
    std::unordered_map<size_t, MassType> newSliceMasses;
    std::unordered_map<size_t, double> newSlicePrices;
    for(const auto& bid : bidTable) {
        assert( bid.bidderIdx < numBidders );

        // steal mass from the slice to which we bid
        stealMassFromSlice(bid.slice, bid.mass);

        size_t objectIdx = bid.slice.getObjectIdx();
        objectsWithBids.insert(objectIdx);
        // bidValue = new price of a slice
        double bidValue = w - getDistToBidder(bidderIdx, objectIdx) + epsilon;
        assert(bidValue > bid.slice.price);

        newSliceMasses[objectIdx] += bid.mass;
#ifdef DEBUG_AUCKTION_KDTREE_GS
        auto findRes = newSlicePrices.find(objectIdx);
        if (findRes != newSliceMasses.end() ) {
            assert( fabs(findRes.second - bidValue ) < 0.00001 * epsilon );
        }
#endif
        newSlicePrices[objectIdx] = bidValue;
    } // loop over bidTable

    // for all objects whose prices changed it may be necessary
    // to update the info in kdtree;
    // also we must create some new slices
    for(size_t objIdx : objectsWithBids) {
        SliceIdx newSliceIdx { bidderIdx, objIdx, true };
        if ( sliceExists(newSliceIdx) ) {
            if ( fabs(getLocalSliceIter(newSliceIdx)->price - newSlicePrices[objIdx] ) >= 0.00001 * epsilon  ) {
                std::cerr << "existing price: " <<  getLocalSliceIter(newSliceIdx)->price << " of slice " << *getLocalSliceIter(newSliceIdx) << ", new price " <<  newSlicePrices[objIdx] << std::endl;
            }
            assert( fabs(getLocalSliceIter(newSliceIdx)->price - newSlicePrices[objIdx] ) < 0.00001 * epsilon );
            changeSliceMass(newSliceIdx, newSliceMasses[objIdx]);
        } else {
            createAssignedSlice(bidderIdx, objIdx, newSliceMasses[objIdx], newSlicePrices[objIdx]);
        }
        //if (not isDiagonal(newSliceIdx)) {
            //kdtree->increase_weight(dnnPointHandles[objIdx], objectSlices[objIdx].begin()->getPrice());
        //}
    }

    for(const auto objIdx : normalObjectsWithChangedPrices) {
        kdtree->increase_weight(dnnPointHandles[objIdx], objectSlices[objIdx].begin()->getPrice());
    }
    // in Gauss-Seidel version all our bids are successful,
    // no unassigned mass remains
#ifdef KEEP_UNASSIGNED_ORDERED
    unassignedBidders.erase(std::make_pair(bidderIdx, allBidders[bidderIdx]), unassignedMass);
#else
    unassignedBidders.erase(bidderIdx, unassignedMass);
#endif

    sanityCheck();
    //std::cout << "exit submitBids" << std::endl;
}

void AuctionKdtreeRunnerGS::reassignSlices(const size_t bidderIdx, const MassType unassignedMass, BidTable& bidTable)
{
    double w = bidTable.getW();
    assert( w < std::numeric_limits<double>::max() );

    sanityCheck();
    std::unordered_set<size_t> objectsWithBids;
    objectsWithBids.reserve( slicesAssignedToBidder[bidderIdx].size() + bidTable.numberOfBids() );
    normalObjectsWithChangedPrices.clear();
    normalObjectsWithChangedPrices.reserve( slicesAssignedToBidder[bidderIdx].size() + bidTable.numberOfBids() );
    // in Gauss-Seidel version we just increase the prices of the slices
    // to which we've been assigned; note that the objects modified here
    // are disjoint from the objects to whose slices we bid
    for(auto& assignedSlice : slicesAssignedToBidder[bidderIdx]) {
        size_t objectIdx = assignedSlice.getObjectIdx();
        double newPrice = w - getDistToBidder(bidderIdx, objectIdx) + epsilon;
        assert( newPrice >= getSlicePrice(assignedSlice) );
        auto oldSliceIter =  getLocalSliceIter(assignedSlice);
        MassType newMass = oldSliceIter->getMass();
        eraseSlice(*oldSliceIter, false); // second argument: do not change unassignedBidders
        createAssignedSlice(bidderIdx, objectIdx, newMass, newPrice);
    }

    // in Gauss-Seidel version we automatically get the objects we want,
    // since in each iteration of the loop we are the only bidder
    std::unordered_map<size_t, MassType> newSliceMasses;
    std::unordered_map<size_t, double> newSlicePrices;
    for(const auto& bid : bidTable) {
        assert( bid.bidderIdx < numBidders );

        // steal mass from the slice to which we bid
        stealMassFromSlice(bid.slice, bid.mass);

        size_t objectIdx = bid.slice.getObjectIdx();
        objectsWithBids.insert(objectIdx);
        // bidValue = new price of a slice
        double bidValue = w - getDistToBidder(bidderIdx, objectIdx) + epsilon;
        assert(bidValue > bid.slice.price);

        newSliceMasses[objectIdx] += bid.mass;
#ifdef DEBUG_AUCKTION_KDTREE_GS
        auto findRes = newSlicePrices.find(objectIdx);
        if (findRes != newSliceMasses.end() ) {
            assert( fabs(findRes.second - bidValue ) < 0.00001 * epsilon );
        }
#endif
        newSlicePrices[objectIdx] = bidValue;
    } // loop over bidTable

    // for all objects whose prices changed it may be necessary
    // to update the info in kdtree;
    // also we must create some new slices
    for(size_t objIdx : objectsWithBids) {
        SliceIdx newSliceIdx { bidderIdx, objIdx, true };
        if ( sliceExists(newSliceIdx) ) {
            if ( fabs(getLocalSliceIter(newSliceIdx)->price - newSlicePrices[objIdx] ) >= 0.00001 * epsilon  ) {
                std::cerr << "existing price: " <<  getLocalSliceIter(newSliceIdx)->price << " of slice " << *getLocalSliceIter(newSliceIdx) << ", new price " <<  newSlicePrices[objIdx] << std::endl;
            }
            assert( fabs(getLocalSliceIter(newSliceIdx)->price - newSlicePrices[objIdx] ) < 0.00001 * epsilon );
            changeSliceMass(newSliceIdx, newSliceMasses[objIdx]);
        } else {
            createAssignedSlice(bidderIdx, objIdx, newSliceMasses[objIdx], newSlicePrices[objIdx]);
        }
        //if (not isDiagonal(newSliceIdx)) {
            //kdtree->increase_weight(dnnPointHandles[objIdx], objectSlices[objIdx].begin()->getPrice());
        //}
    }
    for(const auto objIdx : normalObjectsWithChangedPrices) {
        kdtree->increase_weight(dnnPointHandles[objIdx], objectSlices[objIdx].begin()->getPrice());
    }

    // in Gauss-Seidel version all our bids are successful,
    // no unassigned mass remains
#ifdef KEEP_UNASSIGNED_ORDERED
    unassignedBidders.erase(std::make_pair(bidderIdx, allBidders[bidderIdx]), unassignedMass);
#else
    unassignedBidders.erase(bidderIdx, unassignedMass);
#endif

    sanityCheck();
    //std::cout << "exit submitBids" << std::endl;
}

double AuctionKdtreeRunnerGS::getMinEpsilon() const
{
    return maxVal / ( initialEpsToMaxValFactor * pow(epsilonCommonRatio, maxIterNum) );
}

void AuctionKdtreeRunnerGS::runAuction(void)
{
#ifdef PRINT_DETAILED_TIMING
    std::chrono::high_resolution_clock hrClock;
    std::chrono::time_point<std::chrono::high_resolution_clock> startMoment;
    startMoment = hrClock.now();
#endif
    if ( initialEpsilon == 0.0 ) {
        epsilon = maxVal / initialEpsToMaxValFactor;
    } else {
        epsilon = initialEpsilon;
    }
    // relative error
    // choose some initial epsilon
    int iterNum { 0 };
    minPossibleEpsilon = getMinEpsilon();
    bool notDone { false };

    double currentResult;
    std::vector<double> iterResults;
    std::vector<double> iterEstRelErrors;
    std::vector<std::chrono::time_point<std::chrono::high_resolution_clock>> iterTimes;

    //std::cout << "totalMass = " << totalMass << std::endl;
    do {
        sanityCheck();
        runAuctionPhase();
        sanityCheck();
        iterNum++;
        // result is d^q
        currentResult = getDistanceToQthPowerInternal();
        double denominator = currentResult - totalMass * epsilon;
        currentResult = pow(currentResult, 1.0 / wassersteinPower);

#ifdef PRINT_DETAILED_TIMING
        iterResults.push_back(currentResult);
        iterTimes.push_back(hrClock.now());
        std::cout << "Iteration " << iterNum << " finished. ";
        std::cout << "Current result is " << currentResult  << ", epsilon = " << epsilon << std::endl;
        std::cout << "Rounds (cumulative): " << toStringSeparated(numRounds) << std::endl;
        std::cout << "Nodes visited : " << toStringSeparated(kdtree->nodesVisited) << ", nodes rejected: " << toStringSeparated(kdtree->nodesRejected) << std::endl;
#endif

        if ( denominator <= 0 ) {
            //std::cout << "Epsilon is too big." << std::endl;
            notDone = true;
        } else {
            denominator = pow(denominator, 1.0 / wassersteinPower);
            double numerator = currentResult - denominator;
            //std::cout << " numerator: " << numerator << " denominator: " << denominator << std::endl;
#ifdef PRINT_DETAILED_TIMING
            std::cout << " error bound: " << numerator / denominator << std::endl;
#endif
            // if relative error is greater than delta, continue
            notDone = ( numerator / denominator > maxRelError);
        }

        //notDone = notDone and (epsilon >= maxEpsilon);
        //std::cout << "Iteration " << iterNum << " completed. Epsilon = " << epsilon << ", current result = " << currentResult << std::endl;
        //double lowerBound = getLowerBound();
        //if (lowerBound <= 0.0)
            //std::cout << "Lower bound is" << lowerBound << std::endl;
        //else
            //std::cout << "Lower bound is" << lowerBound << ", estimated relative error " << 100 * ( currentResult - lowerBound ) / lowerBound << std::endl;
        // decrease epsilon for the next iteration
        //oracle->setEpsilon( oracle->getEpsilon() / epsilonCommonRatio );
        //if ( iterNum >= 5 ) {
            //std::cout << "Enter ratio: ";
            //std::cin >> epsilonCommonRatio;
        //}
        epsilon /= epsilonCommonRatio;

        if (iterNum > maxIterNum) {
            std::cerr << "Maximum iteration number exceeded, exiting. Current result is:";
            std::cerr << wassersteinDistance << std::endl;
            std::cout << wassersteinDistance << std::endl;
            notDone = false;
        }
        flushAssignment();
        //std::cout << "Assignment flushed" << std::endl;
#ifdef GATHER_STAT_KDTREE
        statKeeper.resetForIteration();
#endif
    } while ( notDone );
#ifdef PRINT_DETAILED_TIMING
    std::cout << "Rounds (cumulative): " << toStringSeparated(numRounds) << std::endl;
    for(size_t iterIdx = 0; iterIdx < iterResults.size(); ++iterIdx) {
        double trueRelError = ( iterResults.at(iterIdx) - currentResult ) / currentResult;
        auto iterCumulativeTime = iterTimes.at(iterIdx) - startMoment;
        std::chrono::duration<double, std::milli> iterTime =  ( iterIdx > 0) ? iterTimes[iterIdx] - iterTimes[iterIdx - 1] : iterTimes[iterIdx] - startMoment;
        //std::chrono::milliseconds iterTime = iterTimes.at(iterIdx) - startMoment;
        std::cout << "iteration " << iterIdx << ", true rel. error " << trueRelError << ", elapsed time " << std::chrono::duration<double, std::milli>(iterCumulativeTime).count() << ", iteration time " << iterTime.count() << std::endl;
    }
#endif
}

double AuctionKdtreeRunnerGS::getDistanceToQthPowerInternal(void)
{
    sanityCheck();
    assert(unassignedBidders.empty());
    double result = 0.0;
    for(size_t objIdx = 0; objIdx < numObjects; ++objIdx) {
        for(auto& slice : objectSlices[objIdx]) {
            result += pow( getDistToBidder(slice.getOwnerIdx(), objIdx), wassersteinPower)  * slice.getMass();
        }
    }
    wassersteinDistance = pow(result, 1.0 / wassersteinPower);
    return result;
}

double AuctionKdtreeRunnerGS::getLowerBound()
{
    sanityCheck();
    assert(unassignedBidders.empty());
    std::vector<double> minPrices;
    double result = 0.0;
    // get min prices and accumulate the weighted sum in result (with minus)
    for(size_t objIdx = 0; objIdx < numObjects; ++objIdx) {
        double minSlicePrice = std::numeric_limits<double>::max();
        for(auto& slice : objectSlices[objIdx]) {
            if ( slice.price < minSlicePrice ) {
                minSlicePrice = slice.price;
            }
        }
        minPrices.push_back(minSlicePrice);
        result -= minSlicePrice * allObjects[objIdx].getMass();
    }
    // get max losss for each bidder
    for(size_t bidderIdx = 0; bidderIdx < numBidders; ++bidderIdx) {
        const auto& bidder = allBidders[bidderIdx];
        double minLoss = std::numeric_limits<double>::min();
        for(size_t objIdx = 0; objIdx < numObjects; ++objIdx) {
            minLoss = std::min(minLoss, pow(getDistToBidder(bidderIdx, objIdx), wassersteinPower) + minPrices[objIdx] );
        }
        result += minLoss * bidder.getMass();
    }


    return result;
}

double AuctionKdtreeRunnerGS::getWassersteinDistance(void)
{
    runAuction();
    return wassersteinDistance;
}

double AuctionKdtreeRunnerGS::getLossPriceHat(size_t bidderIdx, SliceIdx objectSlice)
{
    const size_t objectIdx = objectSlice.objectIdx;
    return getDistToBidder(bidderIdx, objectIdx) + getPriceHat(objectSlice);
}

double AuctionKdtreeRunnerGS::getPriceHat(const SliceIdx& objectSlice)
{
    return objectSlices[objectSlice.getObjectIdx()].begin()->getPrice();
}

void AuctionKdtreeRunnerGS::checkEpsilonCSS()
{
#ifdef DEBUG_AUCTION_KDTREE_GS
    for(size_t bidderIdx = 0; bidderIdx < numBidders; ++bidderIdx) {
        auto& bidder = allBidders[bidderIdx];
        for(const auto& assignedSlice : slicesAssignedToBidder[bidderIdx]) {
            assert(assignedSlice.isAssigned);
            assert(assignedSlice.ownerIdx == bidderIdx);
            auto& assignedObject = allObjects[assignedSlice.objectIdx];
            double currLoss = getLossPriceHat(bidderIdx, assignedSlice);

            //MassType currMass = getSliceMass(assignedSlice);
            for(size_t otherObjectIdx = 0; otherObjectIdx < numObjects; otherObjectIdx++) {
                if (otherObjectIdx == assignedSlice.objectIdx) {
                    continue;
                }

                auto& slices = objectSlices[otherObjectIdx];
                for(auto objSliceIter = slices.begin(); objSliceIter != slices.end(); ++objSliceIter) {
                    double alternativeLoss = getLossPriceHat(bidderIdx, objSliceIter->sliceIdx);
                    if (currLoss > alternativeLoss + epsilon + 1e-12) {
                        std::cerr << "CSS violated: bidderIdx = " << bidderIdx << ", bidder: " << bidder << std::endl;
                        std::cerr << "currLoss = : " << currLoss << std::endl;
                        std::cerr << " dist to current match = " << distLInf(bidder, assignedObject) << std::endl;
                        std::cerr << "assigned to  : " << assignedSlice << ", object: " << assignedObject << std::endl;
                        std::cerr << "alternativeLoss  : " << alternativeLoss << std::endl;
                        std::cerr << "attained at slice : " << *objSliceIter << ", slice price = " <<  getSlicePrice(objSliceIter->sliceIdx) << std::endl;
                        std::cerr << "attained at object : " << allObjects[objSliceIter->getObjectIdx()];
                        std::cerr << " objectIdx : " << objSliceIter->getObjectIdx() << ", or " << otherObjectIdx << std::endl;
                        std::cerr << "epsilon =  " << epsilon << std::endl;
                        std::cerr << " dist to alternative = " << distLInf(bidder, allObjects[otherObjectIdx]) << std::endl;
                        std::cerr << " currLoss - alternativeLoss - epsilon = " << currLoss - alternativeLoss - epsilon << std::endl;
                        //std::cerr <<  *this << std::endl;
                    }
                    assert( currLoss <= alternativeLoss + epsilon + 1e-12 );
                }
            }
        }
    }
    //std::cout << "epsilon-css OK" << std::endl;
#endif
}

void AuctionKdtreeRunnerGS::sanityCheck()
{
#ifdef DEBUG_AUCTION_KDTREE_GS

    //std::cout << "Entered sanityCheck" << std::endl;

    // count number of slices
    // and check validity of iterators
    {
        assert(globalSliceIdxToIterMap.size() == globalSlices.size());
        assert(globalSliceIdxToIterMap.size() == localSliceIdxToIterMap.size());
        for(auto dsi = globalSlices.begin(); dsi != globalSlices.end(); ++dsi) {
            assert( dsi == globalSliceIdxToIterMap[dsi->getSliceIdx()] );
        }

        int numObjSlices { 0 };
        for(const auto& slices : objectSlices) {
            numObjSlices += slices.size();
            for(auto si = slices.begin(); si != slices.end(); ++si) {
                assert( si == localSliceIdxToIterMap[si->getSliceIdx()] );
            }
        }
        assert(numObjSlices == static_cast<int>(localSliceIdxToIterMap.size()) );

        // check that local and global heaps agree
        for(const auto& sip : localSliceIdxToIterMap) {
            SliceIdx si = sip.first;
            assert( *(globalSliceIdxToIterMap[si]) == *(sip.second) );
        }
    }


    // check mass constraints
    // and correct ordering in slice heap

    std::vector<MassType> bidderMasses(numBidders, 0);
    for(size_t objIdx = 0; objIdx < numObjects; ++objIdx) {
        auto& slices = objectSlices[objIdx];
        MassType totalSlicesMass { 0 };
        for(auto objSliceIter = slices.begin(); objSliceIter != slices.end(); ++objSliceIter) {
            assert( objSliceIter->getMass() > 0 );
            totalSlicesMass += objSliceIter->getMass();
            if ( std::next(objSliceIter) != slices.end() ) {
                assert( objSliceIter->getPrice() <= std::next(objSliceIter)->getPrice() );
            }
            if (objSliceIter->getIsAssigned()) {
                bidderMasses.at(objSliceIter->getOwnerIdx()) += objSliceIter->getMass();
            }
        }
        if (totalSlicesMass != allObjects.at(objIdx).getMass()) {
            std::cerr << "objIdx = " << objIdx << ", current state: " << *this << std::endl;
            std::cerr << "totalSlicesMass = " << totalSlicesMass << ", real mass is " << allObjects[objIdx].getMass() << std::endl;
        }
        assert(totalSlicesMass == allObjects.at(objIdx).getMass());
    }

    //std::cout << "object mass OK, order in slice heap OK" << std::endl;

    // check that each bidder has not more slices than allowed
    // and validity of unassignedBidders
    for(size_t bidderIdx = 0; bidderIdx < numBidders; ++bidderIdx) {
        assert(bidderMasses[bidderIdx] <= allBidders[bidderIdx].getMass());
#ifndef KEEP_UNASSIGNED_ORDERED
        assert(static_cast<MassType>(unassignedBidders.multiplicity(bidderIdx)) == allBidders.at(bidderIdx).getMass() - bidderMasses.at(bidderIdx));
#endif
        MassType sabMass = 0;
        for(const auto& s : slicesAssignedToBidder[bidderIdx]) {
            sabMass += getSliceMass(s);
        }
        assert(bidderMasses[bidderIdx] == sabMass);
    }

    //std::cout << "bidder masses OK" << std::endl;

    // check diagonal heap
    double prevDiagLoss = std::numeric_limits<double>::lowest();
    for(auto globalSliceIter = globalSlices.begin(); globalSliceIter != globalSlices.end(); ++globalSliceIter) {
        if (prevDiagLoss > globalSliceIter->getLossForDiagonal()) {
            std::cerr << *this << std::endl;
            std::cerr << "prevDiagLoss = " << prevDiagLoss << ", current loss = " << globalSliceIter->getLossForDiagonal() << " attained at " << *globalSliceIter << std::endl;
        }
        assert( prevDiagLoss <= globalSliceIter->getLossForDiagonal() );
        prevDiagLoss = globalSliceIter->getLossForDiagonal();
    }
    //std::cout << "diagSlices OK" << std::endl;
    //std::cout << "all possible slices OK, sanityCheck OK" << std::endl;
#endif
}

} // end of namespace auction_sop
