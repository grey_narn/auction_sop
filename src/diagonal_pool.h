#ifndef DIAGONAL_POOL_H
#define DIAGONAL_POOL_H

#include <vector>
#include <numeric>

#include "basic_defs.h"
#include "bid_table.h"



namespace dnn {


class AuctionKdtreeRunner;
    
class DiagonalPool {
public:

    // data
    bool isAssigned;
    int numNormalBidders;
    //int numDiagBidders;
    //int numDiagObjects;
    int totalMass;
    double commonPrice;
    //std::vector<MassType> assignedMasses; // for normal bidder i assignedMasses[i] shows how much mass from this pool (from the projection of i) was assigned to bidder i
    std::vector<MassType> masses; // for diagonal object k objectMasses[k] shows how much mass from object k is in the pool
    MassType deficitMass;
    MyMultiset<size_t> objectIndices;
    ObjectBidTable bidTable;
    AuctionKdtreeRunner* auctionRunner;

    // methods
    DiagonalPool(std::vector<MassType>& diagMasses, bool _isAssigned, double _price);
    double getCommonPrice() const;
    void setCommonPrice(double newValue);
    MassType getAvailableMass(size_t bidderIdx);
    // return -1, if there are more than 1 diagonal object in the pool, else
    // return the index of the single object in the pool
    int objectIdx();
    void submitBid(const ObjectBid& ob);
    size_t getId() const { return id; }
    void setId(size_t newId) { id = newId; }
    //void reassign(std::vector<DiagonalPool>& newPools, MassMatrix& newSliceMasses, RealMatrix newSlicePrices);
    void mergeWithPool(DiagonalPool& anotherPool);
    std::vector<PointSlice> slices;
private:
    size_t id;
};

struct CompDiagonalPools{
    bool operator()(const DiagonalPool& p1, const DiagonalPool& p2) const {
        return p1.getCommonPrice() < p2.getCommonPrice();
    }
};

class DiagonalPoolHeap {
public:
    DiagonalPoolHeap(AuctionKdtreeRunner* _auctionRunner);
    typedef std::vector<DiagonalPool>::iterator PoolIterator;
    std::vector<DiagonalPool> pools;
    //DiagonalPool& getPoolByIdx(size_t poolIdx) { assert(poolIdx < pools.size()); return pools[poolIdx]; }
    PoolIterator ordered_begin();
    PoolIterator ordered_end();
    void reassign();
//private:
    std::vector<std::vector<MassType>> newSliceMasses;
    std::vector<std::vector<double>> newSlicePrices;
    AuctionKdtreeRunner* auctionRunner;
};

}
#endif
